package proxy

import (
	"fmt"
	"net/url"
	"strings"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

func Middleware(baseDomain string, dnsSuffix string) echo.MiddlewareFunc {
	return middleware.ProxyWithConfig(middleware.ProxyConfig{
		Skipper: func(c echo.Context) bool {
			host := c.Request().Host
			if host == fmt.Sprintf("%s.%s", dnsSuffix, baseDomain) ||
				strings.HasPrefix(host, "localhost") {
				return true
			}

			return false
		},
		Balancer: NewDomainBasedBalancer(),
	})
}

type DomainBasedBalancer struct{}

func NewDomainBasedBalancer() *DomainBasedBalancer {
	return &DomainBasedBalancer{}
}

func (b *DomainBasedBalancer) AddTarget(t *middleware.ProxyTarget) bool {
	return true
}

func (b *DomainBasedBalancer) RemoveTarget(name string) bool {
	return true
}

func (b *DomainBasedBalancer) Next(c echo.Context) *middleware.ProxyTarget {
	host := c.Request().Host
	// Take the first part of the hostname
	firstPart := strings.Split(host, ".")[0]

	url, _ := url.Parse(fmt.Sprintf("http://%s.%s", firstPart, firstPart))

	return &middleware.ProxyTarget{
		Name: firstPart,
		URL:  url,
	}
}
