package proxy

import (
	"context"
	"io"

	"golang.org/x/crypto/ssh"
)

func copyRequests(reqChannel <-chan *ssh.Request, conn ssh.Conn) {
	for req := range reqChannel {
		result, payload, err := conn.SendRequest(req.Type, req.WantReply, req.Payload)
		if err != nil {
			continue
		}
		err = req.Reply(result, payload)
		if err != nil {
			continue
		}
	}
}

func copyData(target ssh.Conn, source <-chan ssh.NewChannel) {
	for srcCh := range source {

		ctx := context.Background()
		ctx, cancel := context.WithCancel(ctx)

		go func(s ssh.NewChannel) {
			targetChannel, targetRequestChannel, err := target.OpenChannel(s.ChannelType(), s.ExtraData())
			if err != nil {
				return
			}

			sourceChannel, sourceRequestChannel, err := s.Accept()
			if err != nil {
				return
			}
			defer sourceChannel.Close()

			go func() {
				io.Copy(targetChannel, sourceChannel)
				targetChannel.CloseWrite()
			}()

			go func() {
				io.Copy(sourceChannel, targetChannel)
				sourceChannel.CloseWrite()
			}()

			go func() {
				defer cancel()

				for {
					req, ok := <-targetRequestChannel

					if !ok {
						sourceChannel.Close()
						return
					}

					b, err := sourceChannel.SendRequest(req.Type, req.WantReply, req.Payload)
					if err != nil {
						return
					}
					err = req.Reply(b, nil)
					if err != nil {
						return
					}
				}
			}()

			go func() {
				defer cancel()

				for {
					req, ok := <-sourceRequestChannel

					if !ok {
						targetChannel.Close()
						return
					}

					b, err := targetChannel.SendRequest(req.Type, req.WantReply, req.Payload)
					if err != nil {
						return
					}
					err = req.Reply(b, nil)
					if err != nil {
						return
					}
				}
			}()

			<-ctx.Done()
		}(srcCh)
	}

}
