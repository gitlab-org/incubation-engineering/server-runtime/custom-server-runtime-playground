package proxy

import (
	"bytes"
	"context"
	"fmt"
	"log"
	"net"

	"gitlab.com/patnaikshekhar/experiments/db"
	"golang.org/x/crypto/ssh"
)

const (
	backendPort     = 22
	backendUsername = "root"
)

type SSHProxy struct {
	port   int
	config *ssh.ServerConfig
	db     db.DB
}

func NewSSHProxy(hostKey string, port int32, dbConnection string) (*SSHProxy, error) {

	ctx := context.Background()

	postgresConnection, err := db.NewPostgres(ctx, dbConnection)
	if err != nil {
		return nil, err
	}

	config, err := loadConfig(ctx, hostKey, postgresConnection)
	if err != nil {
		return nil, err
	}

	return &SSHProxy{
		port:   int(port),
		config: config,
		db:     postgresConnection,
	}, nil
}

func loadConfig(ctx context.Context, hostKey string, db db.DB) (*ssh.ServerConfig, error) {
	hostKeySigner, err := ssh.ParsePrivateKey([]byte(hostKey))
	if err != nil {
		return nil, err
	}

	config := &ssh.ServerConfig{
		PublicKeyCallback: func(conn ssh.ConnMetadata, key ssh.PublicKey) (*ssh.Permissions, error) {
			user := conn.User()
			environment, err := db.GetEnvironment(ctx, user)
			if err != nil {
				return nil, err
			}

			expectedKey, _, _, _, err := ssh.ParseAuthorizedKey([]byte(environment.PublicKey))
			if err != nil {
				log.Printf("Error is %s", err)
				return nil, err
			}

			if !bytes.Equal(key.Marshal(), expectedKey.Marshal()) {
				return nil, fmt.Errorf("Invalid public key")
			}

			log.Printf("Authentication for user %s successful", user)

			return &ssh.Permissions{
				Extensions: map[string]string{
					"environmentID": environment.Name,
				},
			}, nil
		},
	}

	config.AddHostKey(hostKeySigner)

	return config, nil
}

func (proxy *SSHProxy) Start() {
	listener, err := net.Listen("tcp", fmt.Sprintf(":%d", proxy.port))
	if err != nil {
		panic(err)
	}

	for {
		conn, err := listener.Accept()
		if err != nil {
			log.Printf("Error accepting connection %s", err)
			continue
		}

		go proxy.handleConnection(conn)
	}
}

func (proxy *SSHProxy) handleConnection(conn net.Conn) {
	log.Printf("Handling new connection")
	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	clientConn, clientChannel, clientReqChannel, err := ssh.NewServerConn(conn, proxy.config)
	if err != nil {
		conn.Close()
		log.Printf("Error accepting connection %s", err)
		return
	}
	defer clientConn.Close()

	if clientConn.Permissions == nil || clientConn.Permissions.Extensions == nil || clientConn.Permissions.Extensions["environmentID"] == "" {
		return
	}

	environmentID := clientConn.Permissions.Extensions["environmentID"]

	remoteAddr := fmt.Sprintf("%s-ssh.%s:%d", environmentID, environmentID, backendPort)
	log.Printf("Creating new connection to address %s", remoteAddr)

	remoteConn, err := net.Dial("tcp", remoteAddr)
	if err != nil {
		conn.Close()
		log.Printf("Error creating backend connection %s", err)
		return
	}

	// pKey, err := ioutil.ReadFile("/keys/testing_key")
	// if err != nil {
	// 	conn.Close()
	// 	remoteConn.Close()
	// 	log.Printf("Could not read key file %s", err)
	// 	return
	// }

	// signer, err := ssh.ParsePrivateKey(pKey)
	// if err != nil {
	// 	conn.Close()
	// 	remoteConn.Close()
	// 	log.Printf("Could not read key file %s", err)
	// 	return
	// }

	backendConn, backendChannel, backendReqChannel, err := ssh.NewClientConn(remoteConn, remoteAddr, &ssh.ClientConfig{
		HostKeyCallback: ssh.InsecureIgnoreHostKey(),
		User:            backendUsername,
		Auth:            []ssh.AuthMethod{},
	})

	if err != nil {
		conn.Close()
		remoteConn.Close()
		log.Printf("Could not read key file %s", err)
		return
	}

	go copyRequests(clientReqChannel, backendConn)
	go copyRequests(backendReqChannel, clientConn)
	go copyData(clientConn, backendChannel)
	go copyData(backendConn, clientChannel)

	go func() {
		clientConn.Wait()
		cancel()
	}()
	go func() {
		backendConn.Wait()
		cancel()
	}()

	<-ctx.Done()
}
