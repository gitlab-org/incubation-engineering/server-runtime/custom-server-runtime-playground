package api

import (
	"context"
	"fmt"
	"io/ioutil"
	"net/http"
)

func makeGitlabRequest(ctx context.Context, accessToken string, method string, uri string) ([]byte, error) {
	req, err := http.NewRequest(method, uri, nil)
	if err != nil {
		return nil, err
	}
	req.Header.Add("Authorization", fmt.Sprintf("Bearer %s", accessToken))
	req = req.WithContext(ctx)

	client := http.DefaultClient
	res, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	if res.StatusCode >= 400 {
		return nil, fmt.Errorf("error in gitlab response, status is %d", res.StatusCode)
	}

	return ioutil.ReadAll(res.Body)
}
