package api

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
)

func GetUser(ctx context.Context, baseURL string, accessToken string) (*GitlabUser, error) {
	uri := fmt.Sprintf("%s/api/v4/user", baseURL)
	res, err := makeGitlabRequest(ctx, accessToken, http.MethodGet, uri)
	if err != nil {
		return nil, err
	}

	var user GitlabUser
	err = json.Unmarshal(res, &user)
	if err != nil {
		return nil, err
	}

	return &user, err
}

type GitlabUser struct {
	ID        int    `json:"id"`
	Username  string `json:"username"`
	Name      string `json:"name"`
	State     string `json:"state"`
	AvatarUrl string `json:"avatar_url"`
	Email     string `json:"email"`
	WebUrl    string `json:"web_url"`
}
