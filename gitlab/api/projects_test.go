package api

import (
	"context"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestGetProjectSuccess(t *testing.T) {
	ctx := context.Background()

	s := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		require.Equal(t, http.MethodGet, r.Method)
		require.Equal(t, "/api/v4/projects/1", r.URL.Path)

		w.Write([]byte("{ \"id\": 1, \"http_url_to_repo\": \"http://example.com/diaspora/diaspora-project-site.git\" }"))
	}))

	val, err := GetProject(ctx, s.URL, "AccessToken", "1")
	require.Nil(t, err)

	require.Equal(t, "http://example.com/diaspora/diaspora-project-site.git", val.HttpUrlToRepo)

	s.Close()
}

func TestGetFileSuccess(t *testing.T) {

	ctx := context.Background()

	s := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		require.Equal(t, http.MethodGet, r.Method)
		require.Equal(t, "/api/v4/projects/1/repository/files/.gitignore", r.URL.Path)
		require.Equal(t, "main", r.URL.Query().Get("ref"))

		w.Write([]byte("{ \"content\": \"dG1w\" }"))
	}))

	val, err := GetFile(ctx, s.URL, "AccessToken", "1", "main", ".gitignore")
	require.Nil(t, err)

	require.Equal(t, "tmp", string(val))

	s.Close()
}

func TestGetFileFailure(t *testing.T) {

	ctx := context.Background()

	s := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		require.Equal(t, http.MethodGet, r.Method)
		require.Equal(t, "/api/v4/projects/1/repository/files/.gitignore", r.URL.Path)
		require.Equal(t, "main", r.URL.Query().Get("ref"))

		w.WriteHeader(http.StatusNotFound)
		w.Header().Add("Content-Type", "application/json")
		w.Write([]byte("{ \"message\": \"404 File Not Found\" }"))
	}))

	_, err := GetFile(ctx, s.URL, "AccessToken", "1", "main", ".gitignore")
	require.NotNil(t, err)

	s.Close()
}
