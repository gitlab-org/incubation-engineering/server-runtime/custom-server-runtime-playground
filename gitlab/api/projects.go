package api

import (
	"context"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"net/http"
)

func GetProject(ctx context.Context, baseURL, accessToken, projectID string) (*GitLabProject, error) {
	u := fmt.Sprintf("%s/api/v4/projects/%s", baseURL, projectID)

	response, err := makeGitlabRequest(ctx, accessToken, http.MethodGet, u)
	if err != nil {
		return nil, err
	}

	var project GitLabProject

	err = json.Unmarshal(response, &project)
	if err != nil {
		return nil, err
	}

	return &project, nil
}

type GitLabProject struct {
	ID            int32  `json:"id"`
	Description   string `json:"description"`
	DefaultBranch string `json:"default_branch"`
	Visibility    string `json:"visibility"`
	SshUrlToRepo  string `json:"ssh_url_to_repo"`
	HttpUrlToRepo string `json:"http_url_to_repo"`
	WebUrl        string `json:"web_url"`
}

func GetFile(
	ctx context.Context,
	baseURL string,
	accessToken string,
	projectID string,
	ref string,
	filename string) ([]byte, error) {
	u := fmt.Sprintf(
		"%s/api/v4/projects/%s/repository/files/%s?ref=%s",
		baseURL,
		projectID,
		filename,
		ref,
	)

	response, err := makeGitlabRequest(ctx, accessToken, http.MethodGet, u)
	if err != nil {
		return nil, err
	}

	var file GitLabFile

	err = json.Unmarshal(response, &file)
	if err != nil {
		return nil, err
	}

	return base64.StdEncoding.DecodeString(file.Content)
}

type GitLabFile struct {
	Content string `json:"content"`
}
