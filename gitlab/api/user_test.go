package api

import (
	"context"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestGetUserSuccess(t *testing.T) {

	ctx := context.Background()

	s := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		require.Equal(t, http.MethodGet, r.Method)
		require.Equal(t, "/api/v4/user", r.URL.Path)

		w.Write([]byte("{ \"username\": \"patnaikshekhar\" }"))
	}))

	val, err := GetUser(ctx, s.URL, "123")
	require.Nil(t, err)

	require.Equal(t, "patnaikshekhar", val.Username)

	s.Close()
}

func TestGetUserFailure(t *testing.T) {

	ctx := context.Background()

	s := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		require.Equal(t, http.MethodGet, r.Method)
		require.Equal(t, "/api/v4/user", r.URL.Path)

		w.WriteHeader(http.StatusNotFound)
		w.Header().Add("Content-Type", "application/json")
		w.Write([]byte("{ \"message\": \"404 File Not Found\" }"))
	}))

	_, err := GetUser(ctx, s.URL, "123")
	require.NotNil(t, err)

	s.Close()
}

func TestGetUserInvalidJSONResponse(t *testing.T) {

	ctx := context.Background()

	s := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		require.Equal(t, http.MethodGet, r.Method)
		require.Equal(t, "/api/v4/user", r.URL.Path)

		w.Write([]byte("{ \"username\": patnaikshekhar }"))
	}))

	_, err := GetUser(ctx, s.URL, "123")
	require.NotNil(t, err)

	s.Close()
}

func TestIntegrationGetUser(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping integration test")
	}

	accessToken := os.Getenv("GITLAB_PAT")

	ctx := context.Background()
	user, err := GetUser(ctx, "https://gitlab.com", accessToken)
	require.Nil(t, err)

	require.Equal(t, "shekharpatnaik", user.Username)
	require.Equal(t, "active", user.State)
	require.Equal(t, "Shekhar Patnaik", user.Name)
	require.Equal(t, "https://gitlab.com/shekharpatnaik", user.WebUrl)
}
