package auth

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"strings"
)

const scope = "read_api profile email read_repository write_repository"

func GenerateAuthUrl(settings *OAuth2Settings, state string) string {
	url := fmt.Sprintf(
		"%s/oauth/authorize?client_id=%s&redirect_uri=%s&response_type=code&scope=%s",
		settings.BaseUrl,
		settings.ClientID,
		settings.RedirectUri,
		scope,
	)

	if state != "" {
		url = fmt.Sprintf("%s&state=%s", url, state)
	}

	return url
}

func GetToken(settings *OAuth2Settings, code string) (string, error) {
	u := fmt.Sprintf("%s/oauth/token", settings.BaseUrl)
	form := url.Values{
		"client_id":     []string{settings.ClientID},
		"client_secret": []string{settings.ClientSecret},
		"redirect_uri":  []string{settings.RedirectUri},
		"code":          []string{code},
		"grant_type":    []string{"authorization_code"},
	}

	req, err := http.NewRequest(http.MethodPost, u, strings.NewReader(form.Encode()))
	if err != nil {
		return "", err
	}

	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	client := http.Client{}
	res, err := client.Do(req)
	if err != nil {
		return "", err
	}

	var response OAuthResponse

	err = json.NewDecoder(res.Body).Decode(&response)
	if err != nil {
		return "", err
	}

	return response.AccessToken, nil
}
