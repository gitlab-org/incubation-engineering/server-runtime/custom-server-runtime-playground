package auth

import (
	"fmt"
	"log"
	"net/http"
	"net/url"
	"strings"
	"time"

	b64 "encoding/base64"

	"github.com/labstack/echo/v4"
)

const (
	CookieName      = "glide-session"
	MissingAuthCode = "Missing Auth Code"
	MissingState    = "Missing state"
)

func EchoMiddleware(settings *OAuth2Settings) echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {

			path := c.Request().URL.Path

			// If calling the auth callback then don't check
			if path == "/auth/callback" || path == "/auth/redirect" || strings.HasPrefix(path, "/api/v1/authorization") {
				return next(c)
			}

			// log.Printf("Host = %s, cookies are %+v", c.Request().Host, c.Cookies())

			// Check Cookie
			cookie, err := c.Request().Cookie(CookieName)

			// If Cookie is not present then redirect
			if err != nil || cookie.Value == "" {
				if strings.HasPrefix(path, "/api") {
					return c.String(http.StatusForbidden, "Forbidden")
				}

				state := b64.StdEncoding.EncodeToString([]byte(getStateUri(c.Request())))
				return c.Redirect(http.StatusTemporaryRedirect, GenerateAuthUrl(settings, state))
			}

			// TODO: If Cookie is present check validity
			return next(c)
		}
	}
}

func HandleAuthCallback(settings *OAuth2Settings) echo.HandlerFunc {
	return func(c echo.Context) error {
		query := c.Request().URL.Query()
		code := query.Get("code")
		if code == "" {
			return c.String(http.StatusInternalServerError, MissingAuthCode)
		}

		state := query.Get("state")
		if state == "" {
			return c.String(http.StatusInternalServerError, MissingState)
		}

		// Get the access token
		token, err := GetToken(settings, code)
		if err != nil {
			return err
		}

		// TODO: encrypt cookie before sending
		c.SetCookie(&http.Cookie{
			Name:     CookieName,
			Value:    token,
			Path:     "/",
			Expires:  time.Now().Add(8 * time.Hour),
			HttpOnly: false,
			SameSite: 0,
		})

		// Redirect to location specified in state
		url, err := b64.StdEncoding.DecodeString(state)
		if err != nil {
			return err
		}

		return c.Redirect(http.StatusTemporaryRedirect, string(url))
	}
}

func HandleAuthCallbackRedirect(e echo.Context) error {
	state := e.QueryParam("state")
	stateUrl := strings.Join(strings.Split(state, ":")[1:], ":")
	u, err := url.Parse(stateUrl)
	if err != nil {
		return err
	}

	newUrl := fmt.Sprintf("%s://%s/oauth2/callback?%s", u.Scheme, u.Host, e.QueryString())
	log.Printf("Redirecting to %s", newUrl)

	return e.Redirect(http.StatusTemporaryRedirect, newUrl)
}

func getStateUri(req *http.Request) string {
	scheme := "http"

	if req.TLS != nil {
		scheme = "https"
	}

	host := req.Host
	path := req.URL.Path
	queryStrings := req.URL.RawQuery

	// completeUrl := fmt.Sprintf("%s://%s%s?%s", scheme, host, path, queryStrings)
	// log.Printf("completeUrl is %s and query strings are %s", completeUrl, queryStrings)

	return fmt.Sprintf("%s://%s%s?%s", scheme, host, path, queryStrings)
}
