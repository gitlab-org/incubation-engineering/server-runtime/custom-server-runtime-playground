package auth

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/require"
)

func baseSettings() *OAuth2Settings {
	return &OAuth2Settings{
		BaseUrl:      "https://gitlab.example.com",
		ClientID:     "123",
		ClientSecret: "456",
		RedirectUri:  "http://localhost:3000",
	}
}
func TestRedirect(t *testing.T) {
	tt := []struct {
		name           string
		settings       *OAuth2Settings
		state          string
		expectedResult string
	}{
		{
			"Url without state",
			baseSettings(),
			"",
			fmt.Sprintf(
				"https://gitlab.example.com/oauth/authorize?client_id=123&redirect_uri=http://localhost:3000&response_type=code&scope=%s",
				scope,
			),
		},
		{
			"Url with state",
			baseSettings(),
			"hello",
			fmt.Sprintf(
				"https://gitlab.example.com/oauth/authorize?client_id=123&redirect_uri=http://localhost:3000&response_type=code&scope=%s&state=hello",
				scope,
			),
		},
	}

	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			result := GenerateAuthUrl(tc.settings, tc.state)
			if result != tc.expectedResult {
				t.Errorf("Expected %s got %s", tc.expectedResult, result)
			}
		})
	}
}

func TestGetToken(t *testing.T) {
	m := StartMockServer(t)
	m.Response = &OAuthResponse{
		AccessToken: "Test",
	}

	settings := &OAuth2Settings{
		BaseUrl:      m.Server.URL,
		ClientID:     "123",
		ClientSecret: "456",
		RedirectUri:  "http://localhost/auth/callback",
	}

	token, err := GetToken(settings, "789")
	require.Nil(t, err)

	require.Equal(t, m.Response.AccessToken, token)
	require.Equal(t, settings.ClientID, m.LastRequest.Form.Get("client_id"))
	require.Equal(t, settings.ClientSecret, m.LastRequest.Form.Get("client_secret"))
	require.Equal(t, settings.RedirectUri, m.LastRequest.Form.Get("redirect_uri"))
	require.Equal(t, "789", m.LastRequest.Form.Get("code"))
}
