package auth

type OAuth2Settings struct {
	BaseUrl      string
	ClientID     string
	ClientSecret string
	RedirectUri  string
}
