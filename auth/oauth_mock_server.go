package auth

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/require"
)

type MockServer struct {
	Server      *httptest.Server
	LastRequest *http.Request
	Response    *OAuthResponse
}

func StartMockServer(t *testing.T) *MockServer {

	m := &MockServer{}

	m.Server = httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		err := r.ParseForm()
		require.Nil(t, err)
		m.LastRequest = r
		err = json.NewEncoder(w).Encode(&m.Response)
		require.Nil(t, err)
	}))

	return m
}
