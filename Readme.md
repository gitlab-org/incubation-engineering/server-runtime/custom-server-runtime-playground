# Custom Control Plane

This repository holds the code for the custom control plane for Server Runtime.

This project is under active development and architectural changes might be made frequently

## About the Server Runtime SEG

The [Server Runtime SEG](https://about.gitlab.com/handbook/engineering/incubation/server-runtime/) within [Incubation Engineering](https://about.gitlab.com/handbook/engineering/incubation/) strives to provide an easy way for GitLab users to be able to run and test their code on the GitLab platform. It enables a remote development experience where you can modify and run your code on any device with access to a browser.

## Goals for the custom control plane

- Can be installed in any Kubernetes Cluster
- Will offer “tight” integration with GitLab
- Can offer a multitude of IDE experiences such as VS Code, Jupyter Hub, VIM, etc both headless and with a UI
- Can be run in our own Kubernetes cluster on GitLab.com as a free or paid offering

## Backlog

The backlog for this project can be accessed [here](https://gitlab.com/gitlab-org/incubation-engineering/server-runtime/meta/-/boards)

## Getting Started

This project can be run in any Kubernetes cluster. The following section shows you how to run this on Google Cloud.

1. Create a GKE cluster on Google Cloud. You'll also need to create a Cloud DNS zone if you are going to use Google as the DNS provider. We will also need to install nginx ingress in the cluster.

```sh
gcloud beta container clusters create "glide-1" \
    --zone "us-central1-c" \
    --no-enable-basic-auth \
    --cluster-version "1.22.12-gke.2300" \
    --release-channel "regular" \
    --machine-type "e2-standard-2" \
    --image-type "COS_CONTAINERD" \
    --disk-type "pd-standard" \
    --disk-size "100" \
    --metadata disable-legacy-endpoints=true \
    --scopes "https://www.googleapis.com/auth/devstorage.read_only","https://www.googleapis.com/auth/logging.write","https://www.googleapis.com/auth/monitoring","https://www.googleapis.com/auth/servicecontrol","https://www.googleapis.com/auth/service.management.readonly","https://www.googleapis.com/auth/trace.append" \
    --max-pods-per-node "110" \
    --spot \
    --num-nodes "2" \
    --logging=SYSTEM,WORKLOAD \
    --monitoring=SYSTEM \
    --enable-ip-alias \
    --network "projects/spatnaik-a11cc4ab/global/networks/default" \
    --subnetwork "projects/spatnaik-a11cc4ab/regions/us-central1/subnetworks/default" \
    --no-enable-intra-node-visibility \
    --default-max-pods-per-node "110" \
    --no-enable-master-authorized-networks \
    --addons HorizontalPodAutoscaling,HttpLoadBalancing,GcePersistentDiskCsiDriver \
    --enable-autoupgrade \
    --enable-autorepair \
    --max-surge-upgrade 1 \
    --max-unavailable-upgrade 0 \
    --enable-shielded-nodes \
    --node-locations "us-central1-c"

gcloud container clusters get-credentials "glide-1" \
    --zone "us-central1-c"

kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/main/deploy/static/provider/cloud/deploy.yaml
```

2. Create a namespace to run the service

```sh
kubectl create ns glide
```

3. Create a secret to hold values needed for the deployment

```sh
kubectl create secret generic glide -n glide \
    --from-literal=REDIRECT_URI=$REDIRECT_URI \
    --from-literal=GITLAB_URL=$GITLAB_URL \
    --from-literal=CLIENT_ID=$CLIENT_ID \
    --from-literal=CLIENT_SECRET=$CLIENT_SECRET \
    --from-literal=GODADDY_API_KEY=$GODADDY_API_KEY \
    --from-literal=GODADDY_API_SECRET=$GODADDY_API_SECRET \
    --from-literal=BASE_DOMAIN=$BASE_DOMAIN \
    --from-literal=DNS_SUFFIX=$DNS_SUFFIX \
    --from-literal=DNS_PROVIDER=$DNS_PROVIDER \
    --from-literal=GOOGLE_PROJECT=$GOOGLE_PROJECT \
    --from-literal=GOOGLE_DNS_ZONE=$GOOGLE_DNS_ZONE \
    --from-literal=DATABASE_URL=$DATABASE_URL \
    --from-file=cert=./certs/tls.crt \
    --from-file=key=./certs/tls.key

kubectl create secret generic googleserviceaccount -n glide \
    --from-file=sa.json=./sa.json

# kubectl create secret tls certs --from-file=./certs/tls.crt --from-file=./certs/tls.key -n glide
kubectl create secret tls certs --cert=./certs/tls.crt --key=./certs/tls.key -n glide
```

The values are as follows
- CLIENT_ID & CLIENT_SECRET- Client ID and secret generated from GitLab
- REDIRECT_URI - Redirect URI for the app. Set this to https://<subdomain>.<your_domain>/auth/callback
- GITLAB_URL - https://gitlab.com for GitLab.com or your own server deployment url
- DNS_PROVIDER - Set this to either godaddy or google
- GODADDY_API_KEY & GODADDY_API_SECRET - An API Key if you are using Godaddy as a DNS provider
- GOOGLE_PROJECT & GOOGLE_DNS_ZONE - The project and zone if you're using Google as a DNS provider
- DATABASE_URL - The postgres database URL
- DNS_SUFFIX - The base DNS suffix. All IDEs will be created with the suffix such as myide.glide.example.com
- BASE_DOMAIN - The base domain name such as example.com
- tls.crt file and tls.key - Multi domain TLS certs shoudl cover both DNS_SUFFIX.BASE_DOMAIN and *.DNS_SUFFIX.BASE_DOMAIN
- sa.json - Google Cloud service account file if using GCP as the DNS provider

4. Deploy the service to the cluster using helm

```sh
make helm
```

5. Access the service by visiting `https://{DNS_SUFFIX}.{BASE_DOMAIN}?project={PROJECT_ID}&ref={BRANCH_REF}}`

## Architecture

The custom control plane or Glide (GitLab IDE) runs as a single service in Kubernetes. It can also potentially run outside of Kubernetes as long as it has the kubeconfig required to deploy pods to a kubernetes cluster.

The architectural components are as follows:

```mermaid
graph TD
    User --> LB
    LB[Load Balancer] --> Glide["Glide (Provisioner)"]
    Glide --> |State Store| Postgres
    Glide --> K8sAPI[Kubernetes API]
    Glide --> DNSAPI[DNS Provider]
    Glide --> |Authentication/API| GitLab
    K8sAPI --> |Provisions| IDE
    LB --> Ingress[Ingress]
    Ingress --> Proxy[OAuth2-Proxy]
    Proxy --> |Authentication| GitLab
    Proxy --> |Authorisation| Glide
    LB --> SSH["SSH Proxy"]
    SSH --> |Authorization| Postgres
    SSH --> IDE
    subgraph Pod
        Proxy-->IDE[IDE Container]
    end
```

- The load balancer can be any external load balancer that will be able to direct network traffic to the main Glide pod.
- All state (environment) details are store in postgres.
- The kubernetes API is used to provision all kubernetes resources
- A new DNS entry is created for each IDE, and therefore
- The main service, GLide interacts with the kubernetes API to provision the IDE
- GLide will also interact with the DNS provider to setup a new DNS entry everytime an IDE is created. Every IDE is provisioned with a new hostname
- GLide interacts with the GitLab API for the following purposes
    - It uses OAuth2 with GitLab as the identity provider to authenticate the user
    - It makes a callout to the users API to fetch username details
    - It makes an API call to the projects endpoint to fetch the URL for the repository (for cloning)
    - It makes an API call to the files endpoint to fetch the .gitlab.experiments.yaml file to provision the environment
- During the provisioning process an ingress is created for each new IDE and directs the traffic to the IDE container
- An IDE pod consists of two containers:
    - The IDE itself which will be running VSCode, Vim, Emacs, etc. This is the container on which the repository is cloned
    - The proxy which is a [fork of oauth2-proxy](https://gitlab.com/gitlab-org/incubation-engineering/server-runtime/oauth2-proxy-fork). This is used for user authentication and authorisation.

The authentication for the IDE uses OAuth2 (for HTTP(s) authentication) with GitLab and looks like the following:

```mermaid
sequenceDiagram
    User ->> Proxy: Access IDE
    Proxy ->> Proxy: Check Cookie
    Proxy ->> GitLab: Cookie not present (Redirect to GitLab)
    GitLab ->> User: Prompt for credentials
    User ->> GitLab: Enter credentials
    GitLab ->> Glide: Redirect with auth code
    Glide ->> Proxy: Redirect based on state to the right domain
    Proxy ->> GitLab: Get Token with Auth Code
    GitLab ->> Proxy: Provide access token
    Proxy ->> Proxy: Generate cookie
    Proxy ->> Glide: Check authorisation for workspace and user
    Glide ->> Proxy: Respond with decision
    Proxy ->> IDE: Fetch assets from backend (IDE
    IDE ->> Proxy: HTML/JS/Websockets
    Proxy ->> User: Display IDE
```

The authentication for the over SSH looks like the following:

```mermaid
sequenceDiagram
    User ->> Provisioner: Request Provision
    Provisioner ->> Provisioner: Generate private public key pair
    Provisioner ->> IDE: Provision IDE + Add private key
    Provisioner ->> Postgres: Store public key
    Provisioner ->> Load_Balancer: Create SSH routes
    User ->> Load_Balancer: Access IDE via browser
    Load_Balancer ->> OAuth2_Proxy: Authenticate User
    OAuth2_Proxy ->> IDE: Display private key
    User ->> Load_Balancer: Run SSH command with Private Key
    Load_Balancer ->> SSH_Proxy: Forward request
    SSH_Proxy ->> Postgres: Fetch Public Key based on IDE being accessed
    SSH_Proxy ->> SSH_Proxy: Validate key passed in request
    SSH_Proxy ->> IDE: Proxy request
```

The provisioning process is as follows:

```mermaid
sequenceDiagram
    Glide ->> Postgres: Create environment
    Glide ->> Kubernetes API: Create new namespace
    Glide ->> Kubernetes API: Create new Persistent Volume Claim
    Glide ->> Kubernetes API: Create new Persistent Volume Claim
    Glide ->> Kubernetes API: Create secret for proxy
    Glide ->> Kubernetes API: Create secret for IDE
    Glide ->> Kubernetes API: Create K8s deployment containing IDE and Proxy
    Glide ->> Kubernetes API: Create K8s service
    Glide ->> Kubernetes API: Create K8s service for each exposed port
    Glide ->> Kubernetes API: Create secret for TLS to pod
    Glide ->> Kubernetes API: Configure ingress for IDE
    Glide ->> Kubernetes API: Configure ingress for each exposed port
    Glide ->> Kubernetes API: Get the loadbalancer IP of the ingress service
    Glide ->> DNS Provider: Configure new DNS entry for IDE
    Glide ->> Kubernetes API: Monitor status of pod until its ready
    Glide ->> Pod: Check whether site is reachable
    Glide ->> Postgres: Update environment status
```

## Development

This project required Go 1.18 or higher. You also need access to a postgres database that you can run in docker. You will also need to set the environment variables in the Makefile

To build and run this project run

```sh
make start-db
make
```

To run all unit tests, use the following:

```sh
make test
```

To run all integration tests, use the following:

```sh
make integration
```

Note: Integration tests will run on a connected cluster.