FROM golang:1.18 as builder
WORKDIR /code

COPY go.mod .
RUN go mod download

COPY . .

# Need node to compile frontend assets
RUN apt-get update && \
    apt-get install -yq --no-install-recommends \
    openssl \
    curl \ 
    wget \
    git \
    gnupg
    # more stuff

RUN curl -fsSL https://deb.nodesource.com/setup_current.x | bash - && \
    apt-get install -y nodejs \
    build-essential && \
    node --version && \ 
    npm --version

RUN cd ui && \
    npm install && \
    npm run build

ARG SKAFFOLD_GO_GCFLAGS
RUN CGO_ENABLED=0 go build -o /experiments main.go

FROM alpine:3
WORKDIR /app
RUN apk --no-cache add ca-certificates \
  && update-ca-certificates
CMD ["./experiments"]
COPY --from=builder /code/ui/dist ./ui/dist
COPY --from=builder /experiments .
