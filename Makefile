
all: clean build run

.PHONY: clean
clean:
	- rm experiments

.PHONY: build
build:
	go build -o experiments main.go

.PHONY: run
run:
	./experiments server \
		--port=$$PORT \
		--client-id=$$CLIENT_ID \
		--client-secret=$$CLIENT_SECRET \
		--gitlab-url=$$GITLAB_URL \
		--redirect-uri=$$REDIRECT_URI \
		--kubeconfig=$$HOME/.kube/config \
		--base-domain=$$BASE_DOMAIN \
		--dns-suffix=$$DNS_SUFFIX \
		--dns-provider=$$DNS_PROVIDER \
		--godaddy-api-key=$$GODADDY_API_KEY \
		--godaddy-api-secret=$$GODADDY_API_SECRET \
		--google-project=$$GOOGLE_PROJECT \
		--google-dns-zone=$$GOOGLE_DNS_ZONE \
		--database-url=$$DATABASE_URL \
		--cert="$$(cat ./certs/tls.crt)" \
		--key="$$(cat ./certs/tls.key)"

.PHONY: run-ssh-proxy
run-ssh-proxy: build
	./experiments sshproxy \
		--database-url=$$DATABASE_URL \
		--host-key="$$(cat ./certs/keys/server_key)"

.PHONY: test
test:
	go test -short -v -cover ./...

.PHONY: integration
integration:
	go test -v -cover ./...

.PHONY: create-cluster
create-cluster:
	./scripts/create_cluster.sh

.PHONY: delete-cluster
delete-cluster:
	doctl kubernetes cluster delete gl-experiments

.PHONY: docker
docker:
	docker build --platform=linux/x86_64 -t patnaikshekhar/glide:$$IMAGE_VERSION . && \
	docker push patnaikshekhar/glide:$$IMAGE_VERSION

.PHONY: open-local
open-local:
	open "http://localhost:3000/?project=40041347&ref=main"

.PHONY: start-db
start-db:
	./scripts/start_db.sh

.PHONY: psql
psql:
	docker exec -it cloudcode-postgres psql -U postgres

.PHONY: helm
helm:
	cd helm && helm dependency update && helm upgrade --install glide -n glide . 

.PHONY: helm-remove
helm-remove:
	cd helm && helm uninstall glide

.PHONY: test-ssh-proxy
test-ssh-proxy:
	ssh -p 2222 shekharpatnaik-3be155be-416e-4f25-8e2c-6a2963c41580@localhost