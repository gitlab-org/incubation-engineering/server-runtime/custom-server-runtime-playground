package routes

import (
	"fmt"
	"log"
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/patnaikshekhar/experiments/auth"
	"gitlab.com/patnaikshekhar/experiments/db"
)

func AuthRedirect(settings *auth.OAuth2Settings) echo.HandlerFunc {
	return func(c echo.Context) error {

		query := c.Request().URL.Query()

		projectID := query.Get("project")
		if projectID == "" {
			return c.String(http.StatusInternalServerError, MissingProjectID)
		}

		ref := query.Get("ref")
		if ref == "" {
			return c.String(http.StatusInternalServerError, MissingRef)
		}

		state := fmt.Sprintf("%s,%s", projectID, ref)

		return c.Redirect(http.StatusTemporaryRedirect, auth.GenerateAuthUrl(settings, state))
	}
}

func CheckAuthorization(db db.DB) echo.HandlerFunc {
	return func(c echo.Context) error {
		id := c.Param("id")
		username := c.QueryParam("username")

		if username == "" {
			return c.String(http.StatusNotFound, "username must be present")
		}

		ctx := c.Request().Context()
		env, err := db.GetEnvironment(ctx, id)
		if err != nil {
			log.Printf("Error in routes.CheckAuthorization %s", err)
			return c.String(http.StatusNotFound, "Unknown error")
		}

		if env.Owner != username {
			return c.String(http.StatusNotFound, "user not valid")
		}

		return c.JSON(http.StatusOK, "")
	}
}
