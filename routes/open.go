package routes

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"github.com/google/uuid"
	"github.com/labstack/echo/v4"
	"gitlab.com/patnaikshekhar/experiments/auth"
	"gitlab.com/patnaikshekhar/experiments/db"
	"gitlab.com/patnaikshekhar/experiments/dns"
	"gitlab.com/patnaikshekhar/experiments/environment"
	"gitlab.com/patnaikshekhar/experiments/gitlab/api"
	"gitlab.com/patnaikshekhar/experiments/k8s"
	"gitlab.com/patnaikshekhar/experiments/provision"
	"gitlab.com/patnaikshekhar/experiments/utils"
)

const (
	experimentsFilename = ".gitlab.experiments.yaml"
	devfileFilename     = "devfile.yaml"
	MissingProjectID    = "Missing Project ID"
	MissingRef          = "Missing Ref"
	MissingCookie       = "Missing Authentication"
)

func OpenEnvironment(
	k8sClient k8s.Client,
	dbClient db.DB,
	dnsProvider dns.DnsProvider,
	baseDomain string,
	dnsSuffix string,
	certFile string,
	keyFile string,
	settings *auth.OAuth2Settings) echo.HandlerFunc {

	return func(c echo.Context) error {
		ctx := c.Request().Context()
		query := c.Request().URL.Query()
		projectID := query.Get("project")
		if projectID == "" {
			return c.String(http.StatusInternalServerError, MissingProjectID)
		}

		ref := query.Get("ref")
		if ref == "" {
			return c.String(http.StatusInternalServerError, MissingRef)
		}

		cookie, err := c.Cookie(auth.CookieName)
		if err != nil {
			return c.String(http.StatusInternalServerError, MissingCookie)
		}

		token := cookie.Value

		envConfigFile, err := getConfigFile(ctx, settings.BaseUrl, token, projectID, ref)
		if err != nil {
			return err
		}

		// Get user details
		user, err := api.GetUser(ctx, settings.BaseUrl, token)
		if err != nil {
			return err
		}

		// Do we have an existing IDE?
		// If the new param is set then always create
		newParam := query.Get("new")
		if newParam != "true" {
			env, _ := dbClient.FindExistingEnvironment(ctx, user.Username, projectID, ref)
			if env != nil && *env.Url != "" {
				redirectUrl := fmt.Sprintf("/index.html?existing=%s&project=%s&ref=%s", *env.Url, projectID, ref)
				return c.Redirect(http.StatusTemporaryRedirect, redirectUrl)
			}
		}

		// Is there more than one IDE?
		ideIndex := 0

		if len(envConfigFile.GetEnvironments()) > 1 {
			ide := query.Get("ide")
			if ide == "" {
				ideList, err := json.Marshal(envConfigFile.GetEnvironments())
				if err != nil {
					return err
				}

				redirectUrl := fmt.Sprintf("/index.html?ides=%s&project=%s&ref=%s", string(ideList), projectID, ref)
				if newParam == "true" {
					redirectUrl = fmt.Sprintf("%s&new=true", redirectUrl)
				}

				return c.Redirect(http.StatusTemporaryRedirect, redirectUrl)
			}

			ideIndex, err = strconv.Atoi(ide)
			if err != nil {
				return err
			}
		}

		// Get Repository Details
		project, err := api.GetProject(ctx, settings.BaseUrl, token, projectID)
		if err != nil {
			return err
		}

		envConfigFile.SelectEnvironment(ideIndex)

		repoUrl := utils.InsertAccessTokenInRepoName(project.HttpUrlToRepo, token)

		environmentID := uuid.New().String()

		// Create a new background context for provisioning
		backgroundContext := context.Background()

		go provision.ProvisionEnvironment(
			backgroundContext, environmentID, k8sClient, dbClient,
			dnsProvider, user.Username, baseDomain, dnsSuffix, envConfigFile, certFile, keyFile, repoUrl,
			projectID, ref, settings, false)

		redirectUrl := fmt.Sprintf("/index.html?environment=%s-%s", user.Username, environmentID)

		return c.Redirect(http.StatusTemporaryRedirect, redirectUrl)
	}
}

func getConfigFile(ctx context.Context, baseUrl, token, projectID, ref string) (environment.EnvironmentConfig, error) {

	var config environment.EnvironmentConfig

	fileContents, err := api.GetFile(
		ctx, baseUrl,
		token, projectID, ref, experimentsFilename)
	if err == nil {
		config, err = environment.ParseExperimentsFile(fileContents)
		return config, err
	}

	fileContents, err = api.GetFile(
		ctx, baseUrl,
		token, projectID, ref, devfileFilename)
	if err == nil {
		config, err = environment.ParseDevfile(fileContents)
		return config, err
	}

	return nil, fmt.Errorf("Neither a experiments file or a devfile was found in the repository")
}
