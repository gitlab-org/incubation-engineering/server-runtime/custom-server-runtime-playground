package routes

import (
	"log"
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/patnaikshekhar/experiments/db"
)

func GetEnvironment(db db.DB) echo.HandlerFunc {
	return func(c echo.Context) error {
		id := c.Param("id")
		ctx := c.Request().Context()
		env, err := db.GetEnvironment(ctx, id)
		if err != nil {
			log.Printf("Error in routes.GetEnvironment %s", err)
			return err
		}

		return c.JSON(http.StatusOK, env)
	}
}
