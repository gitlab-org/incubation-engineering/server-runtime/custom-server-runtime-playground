package k8s

import (
	"context"
	"path/filepath"

	"gitlab.com/patnaikshekhar/experiments/environment"
	"k8s.io/client-go/kubernetes"
	_ "k8s.io/client-go/plugin/pkg/client/auth"
	"k8s.io/client-go/tools/clientcmd"
	"k8s.io/client-go/util/homedir"
)

type Client interface {
	CreateNamespace(ctx context.Context, name string) error
	CreatePVC(ctx context.Context, name string) error
	CreateDeployment(ctx context.Context, name string, namespace string, config environment.EnvironmentConfig) error
	CreateService(ctx context.Context, name string, namespace string, servicePort int, targetPort int) error
	CreateIngress(ctx context.Context, name string, namespace string, baseDomain string, suffix string) error
	CreateTLSSecret(ctx context.Context, name string, certFile string, keyFile string) error
	CreateSecret(ctx context.Context, name string, namespace string, values map[string]string) error
	GetServiceIP(ctx context.Context, name string, namespace string) (string, error)
	GetDeploymentStatus(ctx context.Context, name string) (DeploymentStatus, error)
}

type KubernetesClient struct {
	clientset *kubernetes.Clientset
}

func New(kubeconfig string) (*KubernetesClient, error) {
	config, err := clientcmd.BuildConfigFromFlags("", kubeconfig)
	if err != nil {
		return nil, err
	}

	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		return nil, err
	}

	return &KubernetesClient{
		clientset: clientset,
	}, nil
}

func GetDefaultKubeConfigLocation() string {
	home := homedir.HomeDir()
	return filepath.Join(home, ".kube", "config")
}

type DeploymentStatus string

const (
	DeploymentStatusRunning    DeploymentStatus = "Running"
	DeploymentStatusError      DeploymentStatus = "Error"
	DeploymentStatusNotStarted DeploymentStatus = "NotStarted"
)
