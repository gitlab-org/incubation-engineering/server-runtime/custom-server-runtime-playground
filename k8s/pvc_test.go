package k8s_test

import (
	"context"
	"fmt"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/require"
	"gitlab.com/patnaikshekhar/experiments/k8s"
)

func TestPVCCreation(t *testing.T) {
	if testing.Short() {
		t.Skip("Skipping integration test")
	}

	client, err := k8s.New(k8s.GetDefaultKubeConfigLocation())
	require.Nil(t, err)

	ctx := context.Background()
	name := fmt.Sprintf("patnaikshekhar-%s", uuid.New().String())

	err = client.CreateNamespace(ctx, name)
	require.Nil(t, err)
	defer client.DeleteNamespace(ctx, name)

	err = client.CreatePVC(ctx, name)
	require.Nil(t, err)
	defer client.DeletePVC(ctx, name)

	ns, err := client.GetPVC(ctx, name)
	require.Nil(t, err)
	require.Equal(t, name, ns.Name)
}
