package k8s_test

import (
	"context"
	"fmt"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/require"
	"gitlab.com/patnaikshekhar/experiments/k8s"
)

func TestCreateIngressShouldCreateIngress(t *testing.T) {
	if testing.Short() {
		t.Skip("Skipping integration test")
	}

	client, err := k8s.New(k8s.GetDefaultKubeConfigLocation())
	require.Nil(t, err)

	ctx := context.Background()
	name := fmt.Sprintf("patnaikshekhar-%s", uuid.New().String())

	err = client.CreateNamespace(ctx, name)
	require.Nil(t, err)
	defer client.DeleteNamespace(ctx, name)

	err = client.CreateTLSSecret(ctx, name, "test", "test")
	require.Nil(t, err)
	defer client.DeleteSecret(ctx, k8s.TLSSecretName, name)

	err = client.CreateIngress(ctx, name, name, "example.com", "test")
	require.Nil(t, err)
	defer client.DeleteIngress(ctx, name, name)
}
