package k8s_test

import (
	"context"
	"fmt"
	"log"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/require"
	"gitlab.com/patnaikshekhar/experiments/k8s"
)

func TestCreateSecretShouldCreateSecret(t *testing.T) {
	if testing.Short() {
		t.Skip("Skipping integration test")
	}

	client, err := k8s.New(k8s.GetDefaultKubeConfigLocation())
	require.Nil(t, err)

	ctx := context.Background()
	name := fmt.Sprintf("patnaikshekhar-%s", uuid.New().String())

	err = client.CreateNamespace(ctx, name)
	require.Nil(t, err)
	log.Printf("namespace %s", name)
	defer client.DeleteNamespace(ctx, name)

	err = client.CreateSecret(ctx, name, name, map[string]string{"REPO_URL": "test"})
	require.Nil(t, err)
	defer client.DeleteSecret(ctx, name, name)
}

func TestCreateTLSSecretShouldCreateSecret(t *testing.T) {
	if testing.Short() {
		t.Skip("Skipping integration test")
	}

	client, err := k8s.New(k8s.GetDefaultKubeConfigLocation())
	require.Nil(t, err)

	ctx := context.Background()
	namespace := fmt.Sprintf("patnaikshekhar-%s", uuid.New().String())

	err = client.CreateNamespace(ctx, namespace)
	require.Nil(t, err)
	log.Printf("namespace %s", namespace)
	defer client.DeleteNamespace(ctx, namespace)

	err = client.CreateTLSSecret(ctx, namespace, "test", "test")
	require.Nil(t, err)
	defer client.DeleteSecret(ctx, k8s.TLSSecretName, namespace)
}
