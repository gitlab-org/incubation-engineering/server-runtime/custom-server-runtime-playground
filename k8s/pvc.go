package k8s

import (
	"context"

	v1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

const (
	defaultVolumeSize = "2Gi"
)

func (c *KubernetesClient) CreatePVC(ctx context.Context, name string) error {
	pvcClient := c.clientset.CoreV1().PersistentVolumeClaims(name)
	pvc := &v1.PersistentVolumeClaim{
		ObjectMeta: metav1.ObjectMeta{
			Name: name,
		},
		Spec: v1.PersistentVolumeClaimSpec{
			AccessModes: []v1.PersistentVolumeAccessMode{v1.ReadWriteOnce},
			Resources: v1.ResourceRequirements{
				Requests: v1.ResourceList{
					v1.ResourceStorage: resource.MustParse(defaultVolumeSize),
				},
			},
		},
	}

	_, err := pvcClient.Create(ctx, pvc, metav1.CreateOptions{})
	return err
}

func (c *KubernetesClient) DeletePVC(ctx context.Context, name string) error {
	pvcClient := c.clientset.CoreV1().PersistentVolumeClaims(name)
	err := pvcClient.Delete(ctx, name, metav1.DeleteOptions{})
	return err
}

func (c *KubernetesClient) GetPVC(ctx context.Context, name string) (*v1.PersistentVolumeClaim, error) {
	pvcClient := c.clientset.CoreV1().PersistentVolumeClaims(name)
	pvc, err := pvcClient.Get(ctx, name, metav1.GetOptions{})
	return pvc, err
}
