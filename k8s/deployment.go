package k8s

import (
	"context"
	"fmt"

	"gitlab.com/patnaikshekhar/experiments/environment"
	appsv1 "k8s.io/api/apps/v1"
	apiv1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

const (
	defaultCPU    = "500m"
	defaultMemory = "512Mi"
	proxyImage    = "patnaikshekhar/oauth2-proxy:1"
)

func (c *KubernetesClient) CreateDeployment(ctx context.Context, name string, namespace string, config environment.EnvironmentConfig) error {
	deployClient := c.clientset.AppsV1().Deployments(name)

	deploy, err := config.GetDeployment(name, defaultCPU, defaultMemory)
	if err != nil {
		return err
	}

	deploy.Spec.Template.Spec.Containers = append(deploy.Spec.Template.Spec.Containers, apiv1.Container{
		Name:            "proxy",
		Image:           proxyImage,
		ImagePullPolicy: apiv1.PullAlways,
		EnvFrom: []apiv1.EnvFromSource{
			{
				SecretRef: &apiv1.SecretEnvSource{
					LocalObjectReference: apiv1.LocalObjectReference{
						Name: "proxy",
					},
				},
			},
		},
		Command: []string{
			"/bin/oauth2-proxy",
			"--http-address=http://0.0.0.0:4180",
			"--redirect-url=$(REDIRECT_URI)",
			"--upstream=http://localhost:3000",
			"--email-domain=*",
			"--whitelist-domain=$(WHITELIST_DOMAIN)",
			"--provider=gitlab",
			"--client-id=$(CLIENT_ID)",
			"--client-secret=$(CLIENT_SECRET)",
			"--oidc-issuer-url=$(ISSUER_URL)",
			"--cookie-secret=$(COOKIE_SECRET)",
			"--gitlab-glide-url=$(GLIDE_URL)",
			"--gitlab-glide-workspace-name=$(GLIDE_WORKSPACE_NAME)",
		},
	})
	_, err = deployClient.Create(ctx, deploy, metav1.CreateOptions{})
	return err
}

func (c *KubernetesClient) DeleteDeployment(ctx context.Context, name string) error {
	deployClient := c.clientset.AppsV1().Deployments(name)
	return deployClient.Delete(ctx, name, metav1.DeleteOptions{})
}

func (c *KubernetesClient) GetDeployment(ctx context.Context, name string) (*appsv1.Deployment, error) {
	deployClient := c.clientset.AppsV1().Deployments(name)
	return deployClient.Get(ctx, name, metav1.GetOptions{})
}

func (c *KubernetesClient) GetDeploymentStatus(ctx context.Context, name string) (DeploymentStatus, error) {
	// Find Pod
	podClient := c.clientset.CoreV1().Pods(name)
	podList, err := podClient.List(ctx, metav1.ListOptions{
		LabelSelector: fmt.Sprintf("name=%s", name),
	})

	if err != nil {
		return DeploymentStatusError, err
	}

	// Pod may not have been scheduled
	if len(podList.Items) == 0 {
		return DeploymentStatusNotStarted, nil
	}

	pod := podList.Items[0]
	if pod.Status.Phase == apiv1.PodRunning {
		return DeploymentStatusRunning, nil
	} else if pod.Status.Phase == apiv1.PodFailed {
		return DeploymentStatusError, fmt.Errorf("pod failed to run")
	}

	return DeploymentStatusNotStarted, nil
}
