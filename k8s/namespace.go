package k8s

import (
	"context"

	apiv1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func (c *KubernetesClient) CreateNamespace(ctx context.Context, name string) error {
	ns := &apiv1.Namespace{
		ObjectMeta: metav1.ObjectMeta{
			Name: name,
		},
	}

	nsClient := c.clientset.CoreV1().Namespaces()
	_, err := nsClient.Create(ctx, ns, metav1.CreateOptions{})
	return err
}

func (c *KubernetesClient) DeleteNamespace(ctx context.Context, name string) error {
	nsClient := c.clientset.CoreV1().Namespaces()
	err := nsClient.Delete(ctx, name, metav1.DeleteOptions{})
	return err
}

func (c *KubernetesClient) GetNamespace(ctx context.Context, name string) (*apiv1.Namespace, error) {
	nsClient := c.clientset.CoreV1().Namespaces()
	ns, err := nsClient.Get(ctx, name, metav1.GetOptions{})
	return ns, err
}
