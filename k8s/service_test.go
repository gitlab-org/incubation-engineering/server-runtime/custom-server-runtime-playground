package k8s_test

import (
	"context"
	"fmt"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/require"
	"gitlab.com/patnaikshekhar/experiments/k8s"
)

func TestCreateServiceShouldCreateService(t *testing.T) {
	if testing.Short() {
		t.Skip("Skipping integration test")
	}

	client, err := k8s.New(k8s.GetDefaultKubeConfigLocation())
	require.Nil(t, err)

	ctx := context.Background()
	name := fmt.Sprintf("patnaikshekhar-%s", uuid.New().String())

	err = client.CreateNamespace(ctx, name)
	require.Nil(t, err)
	defer client.DeleteNamespace(ctx, name)

	err = client.CreateService(ctx, name, name, 80, 3000)
	require.Nil(t, err)
	defer client.DeleteService(ctx, name, name)

	deploy, err := client.GetService(ctx, name, name)
	require.Nil(t, err)
	require.Equal(t, name, deploy.Name)
}

func TestGetServiceIP(t *testing.T) {
	if testing.Short() {
		t.Skip("Skipping integration test")
	}

	client, err := k8s.New(k8s.GetDefaultKubeConfigLocation())
	require.Nil(t, err)

	ctx := context.Background()
	ip, err := client.GetServiceIP(ctx, "glide", "glide")
	require.Nil(t, err)

	require.NotEqual(t, "", ip)
}
