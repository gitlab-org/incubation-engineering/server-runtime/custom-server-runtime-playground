package k8s

import (
	"context"
	"fmt"

	"gitlab.com/patnaikshekhar/experiments/environment"
)

type MockK8sClient struct {
	CreatedNamespaceName   string
	CreatedDeploymentName  string
	CreatedDeploymentImage string
	CreatedIngressName     string
	CreatedServiceName     string
	CreatedPVCName         string
	TLSSecretCreated       bool
	CreatedSecretData      map[string]string
	KubernetesFailure      bool
}

func (c *MockK8sClient) CreateNamespace(ctx context.Context, name string) error {
	if c.KubernetesFailure {
		return fmt.Errorf("an error occured")
	}
	c.CreatedNamespaceName = name
	return nil
}

func (c *MockK8sClient) CreatePVC(ctx context.Context, name string) error {
	if c.KubernetesFailure {
		return fmt.Errorf("an error occured")
	}
	c.CreatedPVCName = name
	return nil
}

func (c *MockK8sClient) CreateDeployment(ctx context.Context, name string, namespace string, config environment.EnvironmentConfig) error {
	if c.KubernetesFailure {
		return fmt.Errorf("an error occured")
	}
	c.CreatedDeploymentName = config.GetEnvironments()[0].Name
	c.CreatedDeploymentImage = config.GetEnvironments()[0].Image
	return nil
}

func (c *MockK8sClient) CreateService(ctx context.Context, name string, namespace string, servicePort int, targetPort int) error {
	if c.KubernetesFailure {
		return fmt.Errorf("an error occured")
	}
	c.CreatedServiceName = name
	return nil
}

func (c *MockK8sClient) GetServiceIP(ctx context.Context, name string, namespace string) (string, error) {
	return "1.1.1.1", nil
}

func (c *MockK8sClient) CreateIngress(ctx context.Context, name string, namespace string, baseDomain string, suffix string) error {
	c.CreatedIngressName = name
	return nil
}

func (c *MockK8sClient) GetDeploymentStatus(ctx context.Context, name string) (DeploymentStatus, error) {
	return DeploymentStatusRunning, nil
}

func (c *MockK8sClient) CreateTLSSecret(ctx context.Context, name string, certFile string, keyFile string) error {
	c.TLSSecretCreated = true
	return nil
}

func (c *MockK8sClient) CreateSecret(ctx context.Context, name string, namespace string, values map[string]string) error {
	c.CreatedSecretData = values
	return nil
}
