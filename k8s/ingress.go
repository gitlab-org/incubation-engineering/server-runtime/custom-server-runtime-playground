package k8s

import (
	"context"
	"fmt"

	networkingv1 "k8s.io/api/networking/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func (c *KubernetesClient) CreateIngress(ctx context.Context, name string, namespace string, baseDomain string, suffix string) error {

	domain := fmt.Sprintf("%s.%s.%s", name, suffix, baseDomain)

	ingress := &networkingv1.Ingress{
		ObjectMeta: metav1.ObjectMeta{
			Name: name,
			Annotations: map[string]string{
				"kubernetes.io/ingress.class": "nginx",
			},
		},
		Spec: networkingv1.IngressSpec{
			TLS: []networkingv1.IngressTLS{
				{
					Hosts:      []string{domain},
					SecretName: TLSSecretName,
				},
			},
			Rules: []networkingv1.IngressRule{
				{
					Host: domain,
					IngressRuleValue: networkingv1.IngressRuleValue{
						HTTP: &networkingv1.HTTPIngressRuleValue{
							Paths: []networkingv1.HTTPIngressPath{
								{
									Path:     "/",
									PathType: pathTypePtr(networkingv1.PathTypePrefix),
									Backend: networkingv1.IngressBackend{
										Service: &networkingv1.IngressServiceBackend{
											Name: name,
											Port: networkingv1.ServiceBackendPort{
												Number: 80,
											},
										},
									},
								},
							},
						},
					},
				},
			},
		},
	}
	ingressClient := c.clientset.NetworkingV1().Ingresses(namespace)

	_, err := ingressClient.Create(ctx, ingress, metav1.CreateOptions{})
	return err
}

func (c *KubernetesClient) GetIngress(ctx context.Context, name string, namespace string) (*networkingv1.Ingress, error) {
	ingressClient := c.clientset.NetworkingV1().Ingresses(namespace)
	return ingressClient.Get(ctx, name, metav1.GetOptions{})
}

func (c *KubernetesClient) DeleteIngress(ctx context.Context, name string, namespace string) error {
	ingressClient := c.clientset.NetworkingV1().Ingresses(namespace)
	return ingressClient.Delete(ctx, name, metav1.DeleteOptions{})
}

func pathTypePtr(p networkingv1.PathType) *networkingv1.PathType {
	return &p
}
