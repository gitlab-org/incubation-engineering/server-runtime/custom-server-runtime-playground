package k8s_test

import (
	"context"
	"fmt"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/require"
	"gitlab.com/patnaikshekhar/experiments/environment"
	"gitlab.com/patnaikshekhar/experiments/k8s"
)

func TestCreateDeploymentShouldCreateDeployment(t *testing.T) {
	if testing.Short() {
		t.Skip("Skipping integration test")
	}

	client, name, ctx := createDeployment(t)
	defer client.DeleteDeployment(ctx, name)
	defer client.DeleteSecret(ctx, name, name)
	defer client.DeletePVC(ctx, name)
	defer client.DeleteNamespace(ctx, name)

	deploy, err := client.GetDeployment(ctx, name)
	require.Nil(t, err)
	require.Equal(t, name, deploy.Name)
}

func TestGetDeploymentStatusShouldReturnStatus(t *testing.T) {
	if testing.Short() {
		t.Skip("Skipping integration test")
	}

	client, name, ctx := createDeployment(t)
	defer client.DeleteDeployment(ctx, name)
	defer client.DeleteSecret(ctx, name, name)
	defer client.DeletePVC(ctx, name)
	defer client.DeleteNamespace(ctx, name)

	var status k8s.DeploymentStatus
	var err error

	for {
		status, err = client.GetDeploymentStatus(ctx, name)
		require.Nil(t, err)

		if status != k8s.DeploymentStatusNotStarted {
			break
		}

		time.Sleep(2 * time.Second)
	}

	require.Equal(t, k8s.DeploymentStatusRunning, status)
}

func createDeployment(t *testing.T) (*k8s.KubernetesClient, string, context.Context) {

	client, err := k8s.New(k8s.GetDefaultKubeConfigLocation())
	require.Nil(t, err)

	ctx := context.Background()
	name := fmt.Sprintf("patnaikshekhar-%s", uuid.New().String())

	err = client.CreateNamespace(ctx, name)
	require.Nil(t, err)

	err = client.CreatePVC(ctx, name)
	require.Nil(t, err)

	err = client.CreateSecret(ctx, name, name, map[string]string{
		"REPO_URL":    "http://gitlab.com/test/test",
		"REF":         "main",
		"REPO_DIR":    "test",
		"PRIVATE_KEY": "privatekey",
		"SSH_COMMAND": "ssh test",
	})
	require.Nil(t, err)

	err = client.CreateSecret(ctx, "proxy", name, map[string]string{
		"CLIENT_ID":            "abc",
		"CLIENT_SECRET":        "efg",
		"REDIRECT_URI":         "https://test.com/auth/redirect",
		"WHITELIST_DOMAIN":     "https://test.com/auth/redirect",
		"ISSUER_URL":           "https://gitlab.com",
		"COOKIE_SECRET":        "passwordpassword",
		"GLIDE_URL":            "http://localhost:4000",
		"GLIDE_WORKSPACE_NAME": name,
	})
	require.Nil(t, err)

	err = client.CreateDeployment(ctx, name, name, &environment.ExperimentsConfig{
		Environments: []environment.Environment{
			{Name: "test", Image: "nginx"},
		},
	})
	require.Nil(t, err)

	return client, name, ctx
}
