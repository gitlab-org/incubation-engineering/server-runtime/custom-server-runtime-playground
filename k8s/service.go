package k8s

import (
	"context"

	apiv1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/intstr"
)

const (
	serviceName      = "ingress-nginx-controller"
	serviceNamespace = "ingress-nginx"
)

func (c *KubernetesClient) CreateService(ctx context.Context, name string, namespace string, servicePort int, targetPort int) error {
	svc := &apiv1.Service{
		ObjectMeta: metav1.ObjectMeta{
			Name: name,
		},
		Spec: apiv1.ServiceSpec{
			Selector: map[string]string{
				"name": namespace,
			},
			Type: apiv1.ServiceTypeClusterIP,
			Ports: []apiv1.ServicePort{
				{
					Port:       int32(servicePort),
					TargetPort: intstr.FromInt(targetPort),
				},
			},
		},
	}
	svcClient := c.clientset.CoreV1().Services(namespace)
	_, err := svcClient.Create(ctx, svc, metav1.CreateOptions{})
	return err
}

func (c *KubernetesClient) GetService(ctx context.Context, namespace string, name string) (*apiv1.Service, error) {
	svcClient := c.clientset.CoreV1().Services(namespace)
	return svcClient.Get(ctx, name, metav1.GetOptions{})
}

func (c *KubernetesClient) DeleteService(ctx context.Context, name string, namespace string) error {
	svcClient := c.clientset.CoreV1().Services(namespace)
	return svcClient.Delete(ctx, name, metav1.DeleteOptions{})
}

func (c *KubernetesClient) GetServiceIP(ctx context.Context, name string, namespace string) (string, error) {
	svc, err := c.GetService(ctx, name, namespace)
	if err != nil {
		return "", err
	}

	return svc.Status.LoadBalancer.Ingress[0].IP, nil
}
