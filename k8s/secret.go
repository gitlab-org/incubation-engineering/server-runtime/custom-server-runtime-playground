package k8s

import (
	"context"

	apiv1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

const (
	TLSSecretName = "tls"
)

func (c *KubernetesClient) CreateTLSSecret(ctx context.Context, name string, certFile string, keyFile string) error {
	secretsClient := c.clientset.CoreV1().Secrets(name)
	secret := &apiv1.Secret{
		ObjectMeta: metav1.ObjectMeta{
			Name: TLSSecretName,
		},
		Type: apiv1.SecretTypeTLS,
		StringData: map[string]string{
			"tls.crt": certFile,
			"tls.key": keyFile,
		},
	}

	_, err := secretsClient.Create(ctx, secret, metav1.CreateOptions{})
	return err
}

func (c *KubernetesClient) CreateSecret(ctx context.Context, name string, namespace string, values map[string]string) error {
	secretsClient := c.clientset.CoreV1().Secrets(namespace)
	secret := &apiv1.Secret{
		ObjectMeta: metav1.ObjectMeta{
			Name: name,
		},
		StringData: values,
	}

	_, err := secretsClient.Create(ctx, secret, metav1.CreateOptions{})
	return err
}

func (c *KubernetesClient) DeleteSecret(ctx context.Context, name string, namespace string) error {
	secretsClient := c.clientset.CoreV1().Secrets(namespace)
	err := secretsClient.Delete(ctx, name, metav1.DeleteOptions{})
	return err
}
