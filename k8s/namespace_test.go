package k8s_test

import (
	"context"
	"fmt"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/require"
	"gitlab.com/patnaikshekhar/experiments/k8s"
)

func TestNamespaceCreation(t *testing.T) {
	if testing.Short() {
		t.Skip("Skipping integration test")
	}

	client, err := k8s.New(k8s.GetDefaultKubeConfigLocation())
	require.Nil(t, err)

	ctx := context.Background()
	nsName := fmt.Sprintf("patnaikshekhar-%s", uuid.New().String())

	err = client.CreateNamespace(ctx, nsName)
	require.Nil(t, err)
	defer client.DeleteNamespace(ctx, nsName)

	ns, err := client.GetNamespace(ctx, nsName)
	require.Nil(t, err)
	require.Equal(t, nsName, ns.Name)
}

func TestFailedNamespaceCreation(t *testing.T) {
	if testing.Short() {
		t.Skip("Skipping integration test")
	}

	client, err := k8s.New(k8s.GetDefaultKubeConfigLocation())
	require.Nil(t, err)

	ctx := context.Background()
	nsName := fmt.Sprintf("_invalid_patnaikshekhar-%s", uuid.New().String())

	err = client.CreateNamespace(ctx, nsName)
	require.NotNil(t, err)
}
