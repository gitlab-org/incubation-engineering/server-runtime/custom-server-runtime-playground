package utils

import (
	"fmt"
	"strings"
)

func InsertAccessTokenInRepoName(repoUrl string, token string) string {
	repoUrlWithoutHttp := repoUrl[8:]
	return fmt.Sprintf("https://oauth2:%s@%s", token, repoUrlWithoutHttp)
}

func GetDirectoryNameFromRepoUrl(repoUrl string) string {
	repoComponents := strings.Split(repoUrl, "/")
	lastPart := repoComponents[len(repoComponents)-1]
	return strings.Replace(lastPart, ".git", "", -1)
}
