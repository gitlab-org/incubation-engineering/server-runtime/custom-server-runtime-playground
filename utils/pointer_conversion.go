package utils

func Strp(a string) *string {
	return &a
}

func Int32p(i int32) *int32 {
	return &i
}
