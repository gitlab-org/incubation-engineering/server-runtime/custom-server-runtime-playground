package utils_test

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/patnaikshekhar/experiments/utils"
)

func TestGetDirectoryNameFromRepoUrl(t *testing.T) {
	tt := []struct {
		Name     string
		Input    string
		Expected string
	}{
		{
			Name:     "Basic",
			Input:    "http://example.com/diaspora/diaspora-project-site.git",
			Expected: "diaspora-project-site",
		},
		{
			Name:     "WithToken",
			Input:    "http://oath2:blah@example.com/diaspora/diaspora-project-site.git",
			Expected: "diaspora-project-site",
		},
		{
			Name:     "Empty",
			Input:    "",
			Expected: "",
		},
	}

	for _, test := range tt {
		t.Run(test.Name, func(t *testing.T) {
			result := utils.GetDirectoryNameFromRepoUrl(test.Input)
			require.Equal(t, test.Expected, result)
		})
	}
}
