package dns

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"net/http"
)

const baseURL = "https://api.godaddy.com"

type GoDaddyDnsProvider struct {
	accessKey   string
	accessToken string
	baseDomain  string
	dnsSuffix   string
}

func NewGodaddyProvider(baseDomain string, dnsSuffix string, accessKey string, accessToken string) *GoDaddyDnsProvider {
	return &GoDaddyDnsProvider{
		accessKey:   accessKey,
		accessToken: accessToken,
		baseDomain:  baseDomain,
		dnsSuffix:   dnsSuffix,
	}
}

func (p *GoDaddyDnsProvider) CreateDnsRecord(ctx context.Context, name string, ip string) error {
	url := fmt.Sprintf("%s/v1/domains/%s/records/A/%s.%s", baseURL, p.baseDomain, name, p.dnsSuffix)
	requestmessage := []godaddyDnsRecord{
		{Data: ip},
	}

	b := bytes.NewBuffer(make([]byte, 0, 100))
	err := json.NewEncoder(b).Encode(requestmessage)
	if err != nil {
		return err
	}

	req, err := http.NewRequest(http.MethodPut, url, b)
	if err != nil {
		return err
	}

	req.Header.Add("Authorization", fmt.Sprintf("sso-key %s:%s", p.accessKey, p.accessToken))
	req.Header.Add("Content-Type", "application/json")
	req = req.WithContext(ctx)

	client := http.Client{}

	res, err := client.Do(req)
	if err != nil {
		return err
	}

	if res.StatusCode >= 300 {
		return fmt.Errorf("error from godaddy API %d", res.StatusCode)
	}

	return nil
}

func (p *GoDaddyDnsProvider) GetDnsRecord(ctx context.Context, name string) (string, error) {
	url := fmt.Sprintf("%s/v1/domains/%s/records/A/%s.%s", baseURL, p.baseDomain, name, p.dnsSuffix)

	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return "", err
	}

	req.Header.Add("Authorization", fmt.Sprintf("sso-key %s:%s", p.accessKey, p.accessToken))
	req = req.WithContext(ctx)

	client := http.Client{}

	res, err := client.Do(req)
	if err != nil {
		return "", err
	}

	if res.StatusCode >= 300 {
		return "", fmt.Errorf("error from godaddy API %d", res.StatusCode)
	}

	var record []godaddyDnsRecord

	err = json.NewDecoder(res.Body).Decode(&record)
	if err != nil {
		return "", err
	}

	return record[0].Data, nil
}

func (p *GoDaddyDnsProvider) DeleteDnsRecord(ctx context.Context, name string) error {
	url := fmt.Sprintf("%s/v1/domains/%s/records/A/%s.%s", baseURL, p.baseDomain, name, p.dnsSuffix)

	req, err := http.NewRequest(http.MethodDelete, url, nil)
	if err != nil {
		return err
	}

	req.Header.Add("Authorization", fmt.Sprintf("sso-key %s:%s", p.accessKey, p.accessToken))
	req = req.WithContext(ctx)

	client := http.Client{}

	res, err := client.Do(req)
	if err != nil {
		return err
	}

	if res.StatusCode >= 300 {
		return fmt.Errorf("error from godaddy API %d", res.StatusCode)
	}

	return nil
}

type godaddyDnsRecord struct {
	Data string `json:"data"`
}
