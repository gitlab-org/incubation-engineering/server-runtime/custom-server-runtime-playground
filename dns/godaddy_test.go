package dns_test

import (
	"context"
	"os"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/patnaikshekhar/experiments/dns"
)

func TestGoDaddyDNSCreation(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping integration test")
	}

	baseDomain := os.Getenv("BASE_DOMAIN")
	dnsSuffix := os.Getenv("DNS_SUFFIX")
	accessKey := os.Getenv("GODADDY_API_KEY")
	accessToken := os.Getenv("GODADDY_API_SECRET")
	ctx := context.Background()

	ipAddress := "134.0.0.1"
	recordName := "integration-test"

	provider := dns.NewGodaddyProvider(baseDomain, dnsSuffix, accessKey, accessToken)

	err := provider.CreateDnsRecord(ctx, recordName, ipAddress)
	require.Nil(t, err)
	defer provider.DeleteDnsRecord(ctx, recordName)

	ip, err := provider.GetDnsRecord(ctx, recordName)
	require.Nil(t, err)
	require.Equal(t, ipAddress, ip)
}
