package dns

import (
	"context"
	"fmt"

	"google.golang.org/api/dns/v1"
)

type GoogleProvider struct {
	projectName string
	zone        string
	baseDomain  string
	dnsSuffix   string
}

func NewGoogleProvider(baseDomain string, dnsSuffix string, projectName string, zone string) *GoogleProvider {
	return &GoogleProvider{
		baseDomain:  baseDomain,
		dnsSuffix:   dnsSuffix,
		projectName: projectName,
		zone:        zone,
	}
}

func (p *GoogleProvider) CreateDnsRecord(ctx context.Context, name string, ip string) error {
	svc, err := dns.NewService(ctx)
	if err != nil {
		return err
	}

	_, err = svc.Changes.Create(p.projectName, p.zone, &dns.Change{
		Additions: []*dns.ResourceRecordSet{
			{
				Name:    fmt.Sprintf("%s.%s.%s.", name, p.dnsSuffix, p.baseDomain),
				Type:    "A",
				Rrdatas: []string{ip},
			},
		},
	}).Context(ctx).Do()

	return err
}

func (p *GoogleProvider) DeleteDnsRecord(ctx context.Context, name string, ip string) error {
	svc, err := dns.NewService(ctx)
	if err != nil {
		return err
	}

	_, err = svc.Changes.Create(p.projectName, p.zone, &dns.Change{
		Deletions: []*dns.ResourceRecordSet{
			{
				Name:    fmt.Sprintf("%s.%s.%s.", name, p.dnsSuffix, p.baseDomain),
				Type:    "A",
				Rrdatas: []string{ip},
			},
		},
	}).Context(ctx).Do()

	return err
}
