package dns

import (
	"context"
	"fmt"
)

type DnsProvider interface {
	CreateDnsRecord(ctx context.Context, name string, ip string) error
}

const (
	DnsProviderGoogle  string = "google"
	DnsProviderGodaddy string = "godaddy"
)

func GetProviderByName(dnsProviderName, baseDomain, dnsSuffix, godaddyAPIKey, godaddyAPISecret,
	googleProject, googleDnsZone string) (DnsProvider, error) {

	if dnsProviderName == "google" {
		return NewGoogleProvider(baseDomain, dnsSuffix, googleProject, googleDnsZone), nil
	} else if dnsProviderName == "godaddy" {
		return NewGodaddyProvider(baseDomain, dnsSuffix, godaddyAPIKey, godaddyAPISecret), nil
	}

	return nil, fmt.Errorf("Invalid dns provider name")
}
