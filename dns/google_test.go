package dns_test

import (
	"context"
	"os"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/patnaikshekhar/experiments/dns"
)

func TestGoogleDNSCreation(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping integration test")
	}

	baseDomain := os.Getenv("BASE_DOMAIN")
	dnsSuffix := os.Getenv("DNS_SUFFIX")
	projectName := os.Getenv("GOOGLE_PROJECT")
	zone := os.Getenv("GOOGLE_DNS_ZONE")

	dnsProvider := dns.NewGoogleProvider(baseDomain, dnsSuffix, projectName, zone)

	ctx := context.Background()
	ipAddress := "134.0.0.1"
	recordName := "integration-test"
	err := dnsProvider.CreateDnsRecord(ctx, recordName, ipAddress)
	require.Nil(t, err)
	err = dnsProvider.DeleteDnsRecord(ctx, recordName, ipAddress)
	require.Nil(t, err)
}
