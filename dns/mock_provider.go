package dns

import (
	"context"
	"fmt"
)

type MockDNSProvider struct {
	LastRequestName string
	LastRequestIP   string
}

func (p *MockDNSProvider) CreateDnsRecord(ctx context.Context, name string, ip string) error {
	if name == "error" {
		return fmt.Errorf("an error occurred")
	}
	p.LastRequestIP = ip
	p.LastRequestName = name
	return nil
}
