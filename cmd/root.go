package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
)

var rootCmd = &cobra.Command{
	Use:   "experiments",
	Short: "Gitlab Experiments runs IDEs to edit your code in GitLab",
	Long:  `Gitlab experiments part of Gitlab EE`,
}

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
