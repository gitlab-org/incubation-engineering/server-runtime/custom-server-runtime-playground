package cmd

import (
	"log"

	"github.com/spf13/cobra"
	"gitlab.com/patnaikshekhar/experiments/auth"
	"gitlab.com/patnaikshekhar/experiments/server"
)

var (
	serverCmd = &cobra.Command{
		Use:   "server",
		Short: "Starts the experiments server",
		Long:  `Starts the experiments server`,
		Run: func(cmd *cobra.Command, args []string) {
			server, err := server.New(
				port, certFile, keyFile, oauth2Settings, kubeconfig,
				baseDomain, dnsSuffix, dnsProvider, godaddyAPIKey, godaddyAPISecret,
				googleProject, googleDNSZone, databaseUrl)
			if err != nil {
				log.Fatalf("Error %s", err)
			}
			server.Start()
		},
	}

	port             int32
	oauth2Settings   = &auth.OAuth2Settings{}
	kubeconfig       string
	baseDomain       string
	dnsSuffix        string
	godaddyAPIKey    string
	godaddyAPISecret string
	dnsProvider      string
	googleProject    string
	googleDNSZone    string
	databaseUrl      string
	certFile         string
	keyFile          string
)

func init() {
	serverCmd.Flags().Int32VarP(&port, "port", "p", int32(3000), "port on which the server listens")
	serverCmd.Flags().StringVar(&certFile, "cert", "", "Path to cert file")
	serverCmd.Flags().StringVar(&keyFile, "key", "", "Path to key file")
	serverCmd.Flags().StringVar(&oauth2Settings.BaseUrl, "gitlab-url", "https://gitlab.com", "Gitlab URL")
	serverCmd.Flags().StringVar(&oauth2Settings.ClientID, "client-id", "", "Client ID of app")
	serverCmd.Flags().StringVar(&oauth2Settings.ClientSecret, "client-secret", "", "Client Secret of app")
	serverCmd.Flags().StringVar(&oauth2Settings.RedirectUri, "redirect-uri", "", "RedirectUri")
	serverCmd.Flags().StringVar(&kubeconfig, "kubeconfig", "", "Kubeconfig file")
	serverCmd.Flags().StringVar(&baseDomain, "base-domain", "", "Base domain used to provision DNS entries")
	serverCmd.Flags().StringVar(&dnsSuffix, "dns-suffix", "", "DNS Suffix")
	serverCmd.Flags().StringVar(&dnsProvider, "dns-provider", "", "Type of dns provider can be godaddy,google")
	serverCmd.Flags().StringVar(&godaddyAPIKey, "godaddy-api-key", "", "GoDaddy API Key")
	serverCmd.Flags().StringVar(&godaddyAPISecret, "godaddy-api-secret", "", "GoDaddy API Secret")
	serverCmd.Flags().StringVar(&googleProject, "google-project", "", "Google Project ID")
	serverCmd.Flags().StringVar(&googleDNSZone, "google-dns-zone", "", "Google DNS Zone")
	serverCmd.Flags().StringVarP(&databaseUrl, "database-url", "d", "", "Database URL")

	serverCmd.MarkFlagRequired("client-id")
	serverCmd.MarkFlagRequired("client-secret")
	serverCmd.MarkFlagRequired("redirect-uri")
	serverCmd.MarkFlagRequired("database-url")
	serverCmd.MarkFlagRequired("base-domain")
	serverCmd.MarkFlagRequired("dns-suffix")
	serverCmd.MarkFlagRequired("dns-provider")
	rootCmd.AddCommand(serverCmd)
}
