package cmd

import (
	"log"

	"github.com/spf13/cobra"
	"gitlab.com/patnaikshekhar/experiments/proxy"
)

var (
	sshproxyCmd = &cobra.Command{
		Use:   "sshproxy",
		Short: "Starts the ssh proxy",
		Long:  `Starts the ssh proxy for GitLab experiments`,
		Run: func(cmd *cobra.Command, args []string) {
			sshProxy, err := proxy.NewSSHProxy(hostKey, sshProxyPort, databaseUrl)
			if err != nil {
				log.Fatalf("Error %s", err)
			}
			sshProxy.Start()
		},
	}

	sshProxyPort int32
	hostKey      string
)

func init() {
	sshproxyCmd.Flags().Int32VarP(&sshProxyPort, "port", "p", int32(2222), "port on which the server listens")
	sshproxyCmd.Flags().StringVarP(&hostKey, "host-key", "k", "", "Path to the host key")
	sshproxyCmd.Flags().StringVarP(&databaseUrl, "database-url", "d", "", "Database URL")
	sshproxyCmd.MarkFlagRequired("host-key")
	sshproxyCmd.MarkFlagRequired("database-url")
	rootCmd.AddCommand(sshproxyCmd)
}
