#!/bin/bash

gcloud beta container clusters create "glide-1" \
    --zone "us-central1-c" \
    --no-enable-basic-auth \
    --cluster-version "1.22.12-gke.2300" \
    --release-channel "regular" \
    --machine-type "e2-standard-2" \
    --image-type "COS_CONTAINERD" \
    --disk-type "pd-standard" \
    --disk-size "100" \
    --metadata disable-legacy-endpoints=true \
    --scopes "https://www.googleapis.com/auth/devstorage.read_only","https://www.googleapis.com/auth/logging.write","https://www.googleapis.com/auth/monitoring","https://www.googleapis.com/auth/servicecontrol","https://www.googleapis.com/auth/service.management.readonly","https://www.googleapis.com/auth/trace.append" \
    --max-pods-per-node "110" \
    --spot \
    --num-nodes "2" \
    --logging=SYSTEM,WORKLOAD \
    --monitoring=SYSTEM \
    --enable-ip-alias \
    --network "projects/spatnaik-a11cc4ab/global/networks/default" \
    --subnetwork "projects/spatnaik-a11cc4ab/regions/us-central1/subnetworks/default" \
    --no-enable-intra-node-visibility \
    --default-max-pods-per-node "110" \
    --no-enable-master-authorized-networks \
    --addons HorizontalPodAutoscaling,HttpLoadBalancing,GcePersistentDiskCsiDriver \
    --enable-autoupgrade \
    --enable-autorepair \
    --max-surge-upgrade 1 \
    --max-unavailable-upgrade 0 \
    --enable-shielded-nodes \
    --node-locations "us-central1-c"

gcloud container clusters get-credentials "glide-1" \
    --zone "us-central1-c"

kubectl create ns glide

kubectl create secret generic glide -n glide \
    --from-literal=REDIRECT_URI=$REDIRECT_URI \
    --from-literal=GITLAB_URL=$GITLAB_URL \
    --from-literal=CLIENT_ID=$CLIENT_ID \
    --from-literal=CLIENT_SECRET=$CLIENT_SECRET \
    --from-literal=GODADDY_API_KEY=$GODADDY_API_KEY \
    --from-literal=GODADDY_API_SECRET=$GODADDY_API_SECRET \
    --from-literal=BASE_DOMAIN=$BASE_DOMAIN \
    --from-literal=DNS_SUFFIX=$DNS_SUFFIX \
    --from-literal=DNS_PROVIDER=$DNS_PROVIDER \
    --from-literal=GOOGLE_PROJECT=$GOOGLE_PROJECT \
    --from-literal=GOOGLE_DNS_ZONE=$GOOGLE_DNS_ZONE \
    --from-literal=DATABASE_URL=$DATABASE_URL \
    --from-file=cert=./certs/tls.crt \
    --from-file=key=./certs/tls.key

kubectl create secret generic sshproxy -n glide \
    --from-literal=DATABASE_URL=$DATABASE_URL \
    --from-file=HOST_KEY=./certs/keys/server_key

kubectl create secret generic googleserviceaccount -n glide \
    --from-file=sa.json=./sa.json

kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/main/deploy/static/provider/cloud/deploy.yaml

# kubectl create secret tls certs --from-file=./certs/tls.crt --from-file=./certs/tls.key -n glide
kubectl create secret tls certs --cert=./certs/tls.crt --key=./certs/tls.key -n glide

cd helm && helm install glide -n glide .



# kubectl create secret tls cloudcode-tls --cert=./certs/cloudcode/cert.pem --key=./certs/cloudcode/key.pem
# kubectl create secret tls wildcard-tls --cert=./certs/wildcard/cert.pem --key=./certs/wildcard/key.pem

# kubectl apply -f manifests/