#!/bin/bash

docker rm -vf cloudcode-postgres
docker run --name cloudcode-postgres -e POSTGRES_PASSWORD=postgres -p 5432:5432 -d postgres