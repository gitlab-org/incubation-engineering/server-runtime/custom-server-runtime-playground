const { createVuePlugin } = require('vite-plugin-vue2');

module.exports = {
  plugins: [createVuePlugin()],
  server: {
    port: 4000,
    proxy: {
      '^/api': 'http://localhost:3000',
    }
  }
};
