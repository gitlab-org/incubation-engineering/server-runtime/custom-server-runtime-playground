package db

import "context"

type DB interface {
	CreateEnvironment(ctx context.Context, name string, owner string, projectID string, ref string, publicKey string) error
	UpdateEnvironmentStatus(ctx context.Context, name string, status EnvironmentStatus, subStatus string, errorMessage string, url string) error
	GetEnvironment(ctx context.Context, name string) (*EnvironmentRecord, error)
	FindExistingEnvironment(ctx context.Context, owner string, project string, ref string) (*EnvironmentRecord, error)
}

type EnvironmentStatus string

const (
	EnvironmentStatusInProgress EnvironmentStatus = "In Progress"
	EnvironmentStatusSuccess    EnvironmentStatus = "Success"
	EnvironmentStatusFailure    EnvironmentStatus = "Failure"
)

type EnvironmentRecord struct {
	Name         string             `json:"name"`
	Owner        string             `json:"owner"`
	Status       *EnvironmentStatus `json:"status"`
	SubStatus    *string            `json:"subStatus"`
	ErrorMessage *string            `json:"errorMessage"`
	Url          *string            `json:"url"`
	ProjectID    string             `json:"projectID"`
	Ref          string             `json:"ref"`
	PublicKey    string             `json:"publicKey"`
}

func Envp(e EnvironmentStatus) *EnvironmentStatus {
	return &e
}
