package db

import (
	"context"

	"github.com/jackc/pgx/v4/pgxpool"
)

type PostgresDB struct {
	connection *pgxpool.Pool
}

func NewPostgres(ctx context.Context, databaseUrl string) (*PostgresDB, error) {
	conn, err := pgxpool.Connect(ctx, databaseUrl)
	if err != nil {
		return nil, err
	}

	dbConn := &PostgresDB{connection: conn}

	err = dbConn.createTables(ctx)
	if err != nil {
		return nil, err
	}

	return dbConn, nil
}

func (c *PostgresDB) createTables(ctx context.Context) error {
	_, err := c.connection.Exec(ctx, `
	CREATE TABLE IF NOT EXISTS environments (
		id serial PRIMARY KEY,
		owner text,
		name text,
		project_id text,
		ref text,
		status text,
		sub_status text,
		error_message text,
		url text,
		public_key text
	)
	`)

	return err
}

func (c *PostgresDB) CreateEnvironment(ctx context.Context, name string, owner string, projectID string, ref string, publicKey string) error {
	_, err := c.connection.Exec(ctx, `
		INSERT INTO environments (name, owner, project_id, ref, status, sub_status, error_message, url, public_key) 
		VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9)
	`, name, owner, projectID, ref, EnvironmentStatusInProgress, "", "", "", publicKey)
	return err
}

func (c *PostgresDB) deleteEnvironmentWithName(ctx context.Context, name string) error {
	_, err := c.connection.Exec(ctx, `
		DELETE FROM environments WHERE name = $1
	`, name)

	return err
}

func (c *PostgresDB) UpdateEnvironmentStatus(ctx context.Context, name string, status EnvironmentStatus, subStatus string, errorMessage string, url string) error {
	_, err := c.connection.Exec(ctx, `
		UPDATE environments
		SET status=$1, sub_status=$2, error_message=$3, url=$4
		WHERE NAME = $5
	`, status, subStatus, errorMessage, url, name)

	return err
}

func (c *PostgresDB) GetEnvironment(ctx context.Context, name string) (*EnvironmentRecord, error) {

	env := &EnvironmentRecord{}

	err := c.connection.QueryRow(ctx, `
	SELECT name, owner, project_id, ref, status, sub_status, error_message, url, public_key
	FROM environments
	WHERE name = $1
	`, name).Scan(
		&env.Name, &env.Owner, &env.ProjectID, &env.Ref, &env.Status, &env.SubStatus, &env.ErrorMessage, &env.Url, &env.PublicKey)

	return env, err
}

func (c *PostgresDB) FindExistingEnvironment(ctx context.Context, owner string, project string, ref string) (*EnvironmentRecord, error) {
	env := &EnvironmentRecord{}

	err := c.connection.QueryRow(ctx, `
	SELECT name, owner, project_id, ref, status, sub_status, error_message, url, public_key
	FROM environments
	WHERE owner = $1 and project_id = $2 and ref = $3
	`, owner, project, ref).Scan(
		&env.Name, &env.Owner, &env.ProjectID, &env.Ref, &env.Status, &env.SubStatus, &env.ErrorMessage, &env.Url, &env.PublicKey)

	if err != nil {
		return nil, err
	}

	return env, nil
}
