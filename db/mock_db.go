package db

import (
	"context"
	"fmt"

	"gitlab.com/patnaikshekhar/experiments/utils"
)

type MockDBClient struct {
	Data map[string]*EnvironmentRecord
}

func NewMockDBClient() *MockDBClient {
	return &MockDBClient{
		Data: make(map[string]*EnvironmentRecord),
	}
}

func (c *MockDBClient) CreateEnvironment(ctx context.Context, name string, owner string, projectID string, ref string, publicKey string) error {
	c.Data[name] = &EnvironmentRecord{
		Name:      name,
		Status:    Envp(EnvironmentStatusInProgress),
		Owner:     owner,
		ProjectID: projectID,
		Ref:       ref,
		PublicKey: publicKey,
	}
	return nil
}

func (c *MockDBClient) UpdateEnvironmentStatus(ctx context.Context, name string, status EnvironmentStatus, subStatus string, errorMessage string, url string) error {
	if name == "error" {
		return fmt.Errorf("an error occurred")
	}

	c.Data[name].Status = Envp(status)
	c.Data[name].SubStatus = utils.Strp(subStatus)
	c.Data[name].ErrorMessage = utils.Strp(errorMessage)
	c.Data[name].Url = utils.Strp(url)

	return nil
}

func (c *MockDBClient) FindExistingEnvironment(ctx context.Context, owner string, project string, ref string) (*EnvironmentRecord, error) {
	for _, v := range c.Data {
		if v.Owner == owner && v.ProjectID == project && v.Ref == ref {
			return v, nil
		}
	}

	return nil, nil
}

func (c *MockDBClient) GetEnvironment(ctx context.Context, name string) (*EnvironmentRecord, error) {
	return c.Data[name], nil
}
