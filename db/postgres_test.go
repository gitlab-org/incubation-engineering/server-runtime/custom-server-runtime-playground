package db

import (
	"context"
	"fmt"
	"os"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/require"
)

func TestPostgresConnect(t *testing.T) {
	conn := createConnection(t)
	require.NotNil(t, conn)
}

func TestPostgresCreatesTablesOnInit(t *testing.T) {
	ctx := context.Background()
	conn := createConnection(t)

	_, err := conn.connection.Query(ctx, "SELECT * FROM environments")
	require.Nil(t, err)
}

func TestPostgresCreateEnvironment(t *testing.T) {
	ctx := context.Background()
	conn := createConnection(t)

	name := fmt.Sprintf("patnaikshekhar-%s", uuid.New().String())

	err := conn.CreateEnvironment(ctx, name, "patnaikshekhar", "412090", "main", "pubkey")
	require.Nil(t, err)
	defer conn.deleteEnvironmentWithName(ctx, name)

	var id int
	var environmentName string

	err = conn.connection.
		QueryRow(ctx, "SELECT id, name FROM environments WHERE name = $1", name).Scan(&id, &environmentName)
	require.Nil(t, err)

	require.Equal(t, name, environmentName)
}

func TestPostgresCreateEnvironmentDefaultsStatus(t *testing.T) {
	ctx := context.Background()
	conn := createConnection(t)

	name := fmt.Sprintf("patnaikshekhar-%s", uuid.New().String())

	err := conn.CreateEnvironment(ctx, name, "patnaikshekhar", "412090", "main", "pubkey")
	require.Nil(t, err)
	defer conn.deleteEnvironmentWithName(ctx, name)

	var status string

	err = conn.connection.
		QueryRow(ctx, "SELECT status FROM environments WHERE name = $1", name).Scan(&status)
	require.Nil(t, err)

	require.Equal(t, string(EnvironmentStatusInProgress), status)
}

func TestPostgresUpdateEnvironmentStatusUpdatesTheStatus(t *testing.T) {
	ctx := context.Background()
	conn := createConnection(t)

	name := fmt.Sprintf("patnaikshekhar-%s", uuid.New().String())

	err := conn.CreateEnvironment(ctx, name, "patnaikshekhar", "412090", "main", "pubkey")
	require.Nil(t, err)
	defer conn.deleteEnvironmentWithName(ctx, name)

	conn.UpdateEnvironmentStatus(ctx, name, EnvironmentStatusSuccess, "test", "error", "")

	var status, subStatus, errorMessage string
	err = conn.connection.
		QueryRow(ctx, "SELECT status, sub_status, error_message FROM environments WHERE name = $1", name).
		Scan(&status, &subStatus, &errorMessage)

	require.Nil(t, err)

	require.Equal(t, string(EnvironmentStatusSuccess), status)
	require.Equal(t, "test", subStatus)
	require.Equal(t, "error", errorMessage)
}

func TestGetEnvironmentShouldReturnEnvironmentWhenPresent(t *testing.T) {
	ctx := context.Background()
	conn := createConnection(t)

	name := fmt.Sprintf("patnaikshekhar-%s", uuid.New().String())

	err := conn.CreateEnvironment(ctx, name, "patnaikshekhar", "412090", "main", "pubkey")
	require.Nil(t, err)
	defer conn.deleteEnvironmentWithName(ctx, name)

	env, err := conn.GetEnvironment(ctx, name)
	require.Nil(t, err)

	require.Equal(t, name, env.Name)
}

func TestFindEnvironmentShouldReturnEnvironmentWhenPresent(t *testing.T) {
	ctx := context.Background()
	conn := createConnection(t)
	owner := "patnaikshekhar"
	project := "412090"
	ref := "main"
	publicKey := "pubkey"

	name := fmt.Sprintf("patnaikshekhar-%s", uuid.New().String())

	err := conn.CreateEnvironment(ctx, name, owner, project, ref, publicKey)
	require.Nil(t, err)
	defer conn.deleteEnvironmentWithName(ctx, name)

	env, err := conn.FindExistingEnvironment(ctx, owner, project, ref)
	require.Nil(t, err)
	require.NotNil(t, env)

	require.Equal(t, name, env.Name)
}

func createConnection(t *testing.T) *PostgresDB {
	if testing.Short() {
		t.Skip("skipping integration test")
	}

	databaseUrl := os.Getenv("DATABASE_URL")
	ctx := context.Background()

	conn, err := NewPostgres(ctx, databaseUrl)
	require.Nil(t, err)

	return conn
}
