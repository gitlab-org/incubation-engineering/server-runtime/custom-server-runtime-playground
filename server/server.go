package server

import (
	"context"
	"fmt"
	"log"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"gitlab.com/patnaikshekhar/experiments/auth"
	"gitlab.com/patnaikshekhar/experiments/db"
	"gitlab.com/patnaikshekhar/experiments/dns"
	"gitlab.com/patnaikshekhar/experiments/k8s"
	"gitlab.com/patnaikshekhar/experiments/routes"
)

type Server struct {
	port           int32
	certFile       string
	keyFile        string
	echo           *echo.Echo
	oauth2Settings *auth.OAuth2Settings
	k8sClient      k8s.Client
	dbClient       db.DB
	dnsProvider    dns.DnsProvider
	baseDomain     string
	dnsSuffix      string
}

func New(
	port int32,
	certFile string,
	keyFile string,
	oauth2Settings *auth.OAuth2Settings,
	kubeconfig string,
	baseDomain string,
	dnsSuffix string,
	dnsProviderName string,
	godaddyAPIKey string,
	godaddyAPISecret string,
	googleProject string,
	googleDnsZone string,
	databaseUrl string) (*Server, error) {

	var k8sClient k8s.Client
	var err error

	if kubeconfig != "test_ignore" {
		k8sClient, err = k8s.New(kubeconfig)
		if err != nil {
			return nil, err
		}
	}
	ctx := context.Background()

	dnsProvider, err := dns.GetProviderByName(
		dnsProviderName, baseDomain, dnsSuffix, godaddyAPIKey, godaddyAPISecret, googleProject, googleDnsZone)
	if err != nil {
		return nil, err
	}

	dbClient, err := db.NewPostgres(ctx, databaseUrl)
	if err != nil {
		return nil, err
	}

	return &Server{
		echo:           echo.New(),
		port:           port,
		certFile:       certFile,
		keyFile:        keyFile,
		oauth2Settings: oauth2Settings,
		k8sClient:      k8sClient,
		dnsProvider:    dnsProvider,
		baseDomain:     baseDomain,
		dnsSuffix:      dnsSuffix,
		dbClient:       dbClient,
	}, nil
}

func (s *Server) Start() error {
	s.echo.HideBanner = true

	s.echo.Use(middleware.Logger())
	s.echo.Use(middleware.Recover())
	s.echo.Use(auth.EchoMiddleware(s.oauth2Settings))
	// s.echo.Use(proxy.Middleware(s.baseDomain, s.dnsSuffix))
	s.echo.GET("/auth/callback", auth.HandleAuthCallback(s.oauth2Settings))
	s.echo.GET("/auth/redirect", auth.HandleAuthCallbackRedirect)

	s.echo.GET("/", routes.OpenEnvironment(s.k8sClient, s.dbClient, s.dnsProvider, s.baseDomain, s.dnsSuffix, s.certFile, s.keyFile, s.oauth2Settings))

	s.echo.GET("/api/v1/environments/:id", routes.GetEnvironment(s.dbClient))
	s.echo.GET("/api/v1/authorization/environments/:id", routes.CheckAuthorization(s.dbClient))

	s.echo.Static("/", "ui/dist")

	if s.certFile != "" && s.keyFile != "" {
		err := s.echo.StartTLS(fmt.Sprintf(":%d", s.port), []byte(s.certFile), []byte(s.keyFile))
		if err != nil {
			log.Printf("Error in start tls %s", err)
			return err
		}
	}

	return s.echo.Start(fmt.Sprintf(":%d", s.port))
}

func (s *Server) Stop(ctx context.Context) error {
	return s.echo.Shutdown(ctx)
}
