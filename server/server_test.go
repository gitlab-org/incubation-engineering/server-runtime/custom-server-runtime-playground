package server

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"testing"

	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/patnaikshekhar/experiments/auth"
	"gitlab.com/patnaikshekhar/experiments/db"
	"gitlab.com/patnaikshekhar/experiments/dns"
	"gitlab.com/patnaikshekhar/experiments/k8s"
	"gitlab.com/patnaikshekhar/experiments/utils"
)

func TestStartServer(t *testing.T) {
	s := NewTestServer(4000, &auth.OAuth2Settings{})
	startServer(t, s)
	stopServer(t, s)
}

func TestAuthRedirect(t *testing.T) {
	s := NewTestServer(4050, &auth.OAuth2Settings{
		BaseUrl:      "https://gitlab.example.com",
		ClientID:     "123",
		ClientSecret: "456",
		RedirectUri:  "http://localhost:4050/auth/callback",
	})

	startServer(t, s)
	req, err := http.NewRequest(http.MethodGet, "http://localhost:4050?project=123&ref=main", nil)
	require.Nil(t, err)

	client := &http.Client{
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		},
	}

	res, err := client.Do(req)
	require.Nil(t, err)

	expectedStatusCode := http.StatusTemporaryRedirect
	require.Equal(t, expectedStatusCode, res.StatusCode, "Expected status to be 307")

	location := res.Header.Get("Location")
	expectedLocation := "https://gitlab.example.com/oauth/authorize?client_id=123&redirect_uri=http://localhost:4050/auth/callback&response_type=code&scope=read_api profile email read_repository write_repository"
	require.Contains(t, location, expectedLocation)

	stopServer(t, s)
}

func TestAuthCallback(t *testing.T) {

	port := int32(4070)

	m := auth.StartMockServer(t)
	m.Response = &auth.OAuthResponse{
		AccessToken: "Test",
	}

	settings := &auth.OAuth2Settings{
		BaseUrl:      m.Server.URL,
		ClientID:     "123",
		ClientSecret: "456",
		RedirectUri:  fmt.Sprintf("http://localhost:%d/auth/callback", port),
	}

	s := NewTestServer(port, settings)
	startServer(t, s)

	client := &http.Client{
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		},
	}

	u := fmt.Sprintf("http://localhost:%d/auth/callback?code=123&state=/", port)

	req, err := http.NewRequest(http.MethodGet, u, nil)

	res, err := client.Do(req)
	assert.Nil(t, err)

	cookieValue := res.Header.Get("Set-Cookie")
	expectedCookieValue := fmt.Sprintf("%s=%s", auth.CookieName, m.Response.AccessToken)
	assert.Contains(t, cookieValue, expectedCookieValue)
}

func TestGetEnvironmentShouldReturnEnvironment(t *testing.T) {
	m := StartMockServer(t)

	s := NewTestServer(4090, &auth.OAuth2Settings{
		BaseUrl: m.Server.URL,
	})

	startServer(t, s)
	defer stopServer(t, s)

	req, err := http.NewRequest(http.MethodGet, "http://localhost:4090/api/v1/environments/patnaikshekhar-123", nil)
	require.Nil(t, err)

	req.AddCookie(&http.Cookie{
		Name:  auth.CookieName,
		Value: "test",
	})

	res, err := http.DefaultClient.Do(req)
	require.Nil(t, err)

	require.Equal(t, http.StatusOK, res.StatusCode)

	var record db.EnvironmentRecord
	err = json.NewDecoder(res.Body).Decode(&record)
	require.Nil(t, err)

	require.Equal(t, "patnaikshekhar-123", record.Name)
	require.Equal(t, db.EnvironmentStatusSuccess, *record.Status)
}

func startServer(t *testing.T, s *Server) {
	go func() {
		err := s.Start()
		if err != nil && err != http.ErrServerClosed {
			t.Errorf("Expected nil got %s", err)
		}
	}()
}

func stopServer(t *testing.T, s *Server) {
	ctx := context.Background()
	err := s.Stop(ctx)
	require.Nil(t, err)
}

func NewTestServer(port int32, oauth2Settings *auth.OAuth2Settings) *Server {

	dnsProvider := &dns.MockDNSProvider{}
	dbClient := db.NewMockDBClient()
	dbClient.Data["patnaikshekhar-123"] = &db.EnvironmentRecord{
		Name:         "patnaikshekhar-123",
		Status:       db.Envp(db.EnvironmentStatusSuccess),
		SubStatus:    utils.Strp(""),
		ErrorMessage: utils.Strp(""),
	}

	k8sClient := &k8s.MockK8sClient{}

	return &Server{
		echo:           echo.New(),
		port:           port,
		oauth2Settings: oauth2Settings,
		k8sClient:      k8sClient,
		dnsProvider:    dnsProvider,
		baseDomain:     "",
		dnsSuffix:      "",
		dbClient:       dbClient,
	}
}
