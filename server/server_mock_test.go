package server

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/patnaikshekhar/experiments/auth"
	"gitlab.com/patnaikshekhar/experiments/gitlab/api"
)

type MockServer struct {
	Server             *httptest.Server
	LastAuthRequest    *http.Request
	LastGetFileRequest *http.Request
	LastGetUserRequest *http.Request
	AuthResponse       *auth.OAuthResponse
	GetFileApiContent  string
}

func StartMockServer(t *testing.T) *MockServer {

	m := &MockServer{}

	m.Server = httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.URL.Path == "/auth/token" {
			err := r.ParseForm()
			require.Nil(t, err)
			m.LastAuthRequest = r
			err = json.NewEncoder(w).Encode(&m.AuthResponse)
			require.Nil(t, err)
		} else if strings.HasPrefix(r.URL.Path, "/projects") {
			m.LastGetFileRequest = r
			err := json.NewEncoder(w).Encode(api.GitLabFile{Content: m.GetFileApiContent})
			require.Nil(t, err)
		} else if strings.HasPrefix(r.URL.Path, "/user") {
			m.LastGetUserRequest = r
			err := json.NewEncoder(w).Encode(api.GitlabUser{Username: "patnaikshekhar"})
			require.Nil(t, err)
		}
	}))

	return m
}
