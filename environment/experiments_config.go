package environment

import (
	"gitlab.com/patnaikshekhar/experiments/utils"
	"gopkg.in/yaml.v2"
	appsv1 "k8s.io/api/apps/v1"
	apiv1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// ExperimentsConfig is a very simple format to describe your experiments
type ExperimentsConfig struct {
	Version             string        `yaml:"version"`
	Environments        []Environment `yaml:"environments"`
	Ports               []int         `yaml:"ports"`
	selectedEnvironment int
}

func ParseExperimentsFile(fileContents []byte) (*ExperimentsConfig, error) {
	var experiments ExperimentsConfig

	err := yaml.Unmarshal(fileContents, &experiments)
	if err != nil {
		return nil, err
	}

	return &experiments, nil
}

func (config *ExperimentsConfig) GetPorts() []int {
	return config.Ports
}

func (config *ExperimentsConfig) GetEnvironments() []Environment {
	return config.Environments
}

func (config *ExperimentsConfig) SelectEnvironment(index int) {
	config.selectedEnvironment = index
}

func (config *ExperimentsConfig) GetDeployment(name string, cpu string, memory string) (*appsv1.Deployment, error) {
	deploy := &appsv1.Deployment{
		ObjectMeta: metav1.ObjectMeta{
			Name: name,
		},
		Spec: appsv1.DeploymentSpec{
			Replicas: utils.Int32p(1),
			Selector: &metav1.LabelSelector{
				MatchLabels: map[string]string{
					"name": name,
				},
			},
			Template: apiv1.PodTemplateSpec{
				ObjectMeta: metav1.ObjectMeta{
					Labels: map[string]string{
						"name": name,
					},
				},
				Spec: apiv1.PodSpec{
					Containers: []apiv1.Container{
						{
							Name:            "ide",
							Image:           config.Environments[config.selectedEnvironment].Image,
							ImagePullPolicy: apiv1.PullAlways,
							Env: []apiv1.EnvVar{
								{
									Name: "REPO_URL",
									ValueFrom: &apiv1.EnvVarSource{
										SecretKeyRef: &apiv1.SecretKeySelector{
											LocalObjectReference: apiv1.LocalObjectReference{
												Name: name,
											},
											Key: "REPO_URL",
										},
									},
								},
								{
									Name: "REF",
									ValueFrom: &apiv1.EnvVarSource{
										SecretKeyRef: &apiv1.SecretKeySelector{
											LocalObjectReference: apiv1.LocalObjectReference{
												Name: name,
											},
											Key: "REF",
										},
									},
								},
								{
									Name: "REPO_DIR",
									ValueFrom: &apiv1.EnvVarSource{
										SecretKeyRef: &apiv1.SecretKeySelector{
											LocalObjectReference: apiv1.LocalObjectReference{
												Name: name,
											},
											Key: "REPO_DIR",
										},
									},
								},
								{
									Name: "PRIVATE_KEY",
									ValueFrom: &apiv1.EnvVarSource{
										SecretKeyRef: &apiv1.SecretKeySelector{
											LocalObjectReference: apiv1.LocalObjectReference{
												Name: name,
											},
											Key: "PRIVATE_KEY",
										},
									},
								},
								{
									Name: "SSH_COMMAND",
									ValueFrom: &apiv1.EnvVarSource{
										SecretKeyRef: &apiv1.SecretKeySelector{
											LocalObjectReference: apiv1.LocalObjectReference{
												Name: name,
											},
											Key: "SSH_COMMAND",
										},
									},
								},
							},
							VolumeMounts: []apiv1.VolumeMount{
								{
									MountPath: "/home/workspaces",
									Name:      "main",
								},
							},
							Resources: apiv1.ResourceRequirements{
								Limits: apiv1.ResourceList{
									apiv1.ResourceCPU:    resource.MustParse(cpu),
									apiv1.ResourceMemory: resource.MustParse(memory),
								},
							},
						},
					},
					Volumes: []apiv1.Volume{
						{
							Name: "main",
							VolumeSource: apiv1.VolumeSource{
								PersistentVolumeClaim: &apiv1.PersistentVolumeClaimVolumeSource{
									ClaimName: name,
								},
							},
						},
					},
				},
			},
		},
	}

	return deploy, nil
}
