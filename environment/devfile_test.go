package environment_test

import (
	"io/ioutil"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/patnaikshekhar/experiments/environment"
)

func TestParseShouldParseFile(t *testing.T) {
	contents, err := ioutil.ReadFile("../fixtures/devfiles/nodejs_example.devfile.yaml")
	require.Nil(t, err)

	_, err = environment.ParseDevfile(contents)
	require.Nil(t, err)
}

func TestParseShouldNotParseInvalidFile(t *testing.T) {
	contents, err := ioutil.ReadFile("../fixtures/devfiles/invalid.devfile.yaml")
	require.Nil(t, err)

	_, err = environment.ParseDevfile(contents)
	require.NotNil(t, err)
}

func TestGetPortsShouldReturnPorts(t *testing.T) {

	tt := []struct {
		description string
		filename    string
		ports       []int
	}{
		{"without ports should return none", "../fixtures/devfiles/nodejs_example.devfile.yaml", []int{}},
		{"with ports should return list of ports", "../fixtures/devfiles/example_with_ports.devfile.yaml", []int{8000}},
	}

	for _, tr := range tt {
		t.Run(tr.description, func(t *testing.T) {
			contents, err := ioutil.ReadFile(tr.filename)
			require.Nil(t, err)

			config, err := environment.ParseDevfile(contents)
			require.Nil(t, err)

			require.Equal(t, tr.ports, config.GetPorts())
		})
	}

}

func TestGetPortsShouldNotReturnDuplicatePorts(t *testing.T) {

	tt := []struct {
		description string
		filename    string
		ports       []int
	}{
		{"without ports should return none", "../fixtures/devfiles/nodejs_example.devfile.yaml", []int{}},
		{"with ports should return list of ports", "../fixtures/devfiles/example_with_ports.devfile.yaml", []int{8000}},
		{"with multiple ports should return list of unique ports", "../fixtures/devfiles/example_with_multiple_ports.devfile.yaml", []int{8000, 8001}},
	}

	for _, tr := range tt {
		t.Run(tr.description, func(t *testing.T) {
			contents, err := ioutil.ReadFile(tr.filename)
			require.Nil(t, err)

			config, err := environment.ParseDevfile(contents)
			require.Nil(t, err)

			require.Equal(t, tr.ports, config.GetPorts())
		})
	}

}

func TestGetEnvironmentsShouldReturnValue(t *testing.T) {
	contents, err := ioutil.ReadFile("../fixtures/devfiles/nodejs_example.devfile.yaml")
	require.Nil(t, err)

	config, err := environment.ParseDevfile(contents)
	require.Nil(t, err)

	require.Equal(t, 1, len(config.GetEnvironments()))
}

func TestGetDeploymentShouldContainContainers(t *testing.T) {
	contents, err := ioutil.ReadFile("../fixtures/devfiles/example_with_ports.devfile.yaml")
	require.Nil(t, err)

	config, err := environment.ParseDevfile(contents)
	require.Nil(t, err)

	deploy, err := config.GetDeployment("test", "10", "10")
	require.Nil(t, err)

	require.NotEqual(t, 0, len(deploy.Spec.Template.Spec.Containers))
}

// func TestGetDeploymentShouldInjectIDEContainer(t *testing.T) {
// 	contents, err := ioutil.ReadFile("../fixtures/devfiles/example_with_ports.devfile.yaml")
// 	require.Nil(t, err)

// 	config, err := environment.ParseDevfile(contents)
// 	require.Nil(t, err)

// 	deploy, err := config.GetDeployment("test", "10", "10")
// 	require.Nil(t, err)

// 	require.Equal(t, 2, len(deploy.Spec.Template.Spec.Containers))
// }
