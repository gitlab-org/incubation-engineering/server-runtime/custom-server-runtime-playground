package environment

import (
	appsv1 "k8s.io/api/apps/v1"
)

type EnvironmentConfig interface {
	GetPorts() []int
	GetEnvironments() []Environment
	SelectEnvironment(index int)
	GetDeployment(name string, cpu string, memory string) (*appsv1.Deployment, error)
}

type Environment struct {
	Name  string `yaml:"name"`
	Image string `yaml:"image"`
}
