package environment

import (
	"fmt"

	"github.com/devfile/api/v2/pkg/apis/workspaces/v1alpha2"
	"github.com/devfile/library/pkg/devfile/generator"
	"github.com/devfile/library/pkg/devfile/parser"
	"github.com/devfile/library/pkg/devfile/parser/data/v2/common"
	appsv1 "k8s.io/api/apps/v1"
	apiv1 "k8s.io/api/core/v1"
)

type Devfile struct {
	devfile             parser.DevfileObj
	selectedEnvironment int
}

func ParseDevfile(fileContents []byte) (*Devfile, error) {
	devfile, err := parser.ParseDevfile(parser.ParserArgs{
		Data: fileContents,
	})
	if err != nil {
		return nil, err
	}

	return &Devfile{
		devfile: devfile,
	}, nil
}

func (config *Devfile) GetPorts() []int {

	result := []int{}
	resultSet := make(map[int]interface{})

	components, err := config.devfile.Data.GetComponents(common.DevfileOptions{})
	if err != nil {
		return result
	}

	for _, component := range components {
		if component.Container != nil {
			for _, ep := range component.Container.Endpoints {
				if _, ok := resultSet[ep.TargetPort]; !ok {
					result = append(result, ep.TargetPort)
					resultSet[ep.TargetPort] = nil
				}
			}
		}
	}
	return result
}

func (config *Devfile) GetEnvironments() []Environment {
	// TODO: Remove hardcoded images
	return []Environment{
		{Name: "VS Code", Image: "patnaikshekhar/cloudcode-golang:1"},
	}
}

func (config *Devfile) SelectEnvironment(index int) {
	config.selectedEnvironment = index
}

func (config *Devfile) GetDeployment(name string, cpu string, memory string) (*appsv1.Deployment, error) {

	err := config.devfile.AddEnvVars(map[string][]v1alpha2.EnvVar{
		"che-machine-exec": {
			{Name: "POD_SELECTOR", Value: fmt.Sprintf("name=%s", name)},
			{Name: "DEVWORKSPACE_NAME", Value: name},
		},
	})

	containers, err := generator.GetContainers(config.devfile, common.DevfileOptions{})
	if err != nil {
		return nil, err
	}

	for i, container := range containers {
		containers[i].VolumeMounts = append(container.VolumeMounts, apiv1.VolumeMount{
			MountPath: "/home/workspaces",
			Name:      "main",
		})
		containers[i].EnvFrom = append(containers[i].EnvFrom, apiv1.EnvFromSource{
			SecretRef: &apiv1.SecretEnvSource{
				LocalObjectReference: apiv1.LocalObjectReference{
					Name: name,
				},
			},
		})
	}

	// To generate a Kubernetes deployment of type v1.Deployment
	deployParams := generator.DeploymentParams{
		TypeMeta: generator.GetTypeMeta("Deployment", "apps/v1"),
		ObjectMeta: generator.GetObjectMeta(name, name, map[string]string{
			"name": name,
		}, make(map[string]string)),
		Containers: containers,
		Volumes: []apiv1.Volume{
			{
				Name: "main",
				VolumeSource: apiv1.VolumeSource{
					PersistentVolumeClaim: &apiv1.PersistentVolumeClaimVolumeSource{
						ClaimName: name,
					},
				},
			},
		},
		PodSelectorLabels: map[string]string{
			"name": name,
		},
	}

	deployment, err := generator.GetDeployment(config.devfile, deployParams)
	if err != nil {
		return nil, err
	}

	return deployment, nil
}
