package provision_test

import (
	"context"
	"fmt"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/require"
	"gitlab.com/patnaikshekhar/experiments/auth"
	"gitlab.com/patnaikshekhar/experiments/db"
	"gitlab.com/patnaikshekhar/experiments/dns"
	"gitlab.com/patnaikshekhar/experiments/environment"
	"gitlab.com/patnaikshekhar/experiments/k8s"
	"gitlab.com/patnaikshekhar/experiments/provision"
)

func TestProvisionUpdatesDatabase(t *testing.T) {
	dbClient, _, _, environmentID := clientSetup(false)

	require.Equal(t, 1, len(dbClient.Data))
	require.Equal(t, db.EnvironmentStatusSuccess, *dbClient.Data[environmentID].Status)
}

func TestProvisionCreatesAKubernetesNamespace(t *testing.T) {
	_, k8sClient, _, environmentID := clientSetup(false)
	require.Equal(t, k8sClient.CreatedNamespaceName, environmentID)
}

func TestProvisionCreatesAKubernetesPVC(t *testing.T) {
	_, k8sClient, _, environmentID := clientSetup(false)
	require.Equal(t, k8sClient.CreatedPVCName, environmentID)
}

func TestProvisionUpdatesStatusWhenStepErrorsOut(t *testing.T) {
	dbClient, _, _, environmentID := clientSetup(true)
	require.Equal(t, db.EnvironmentStatusFailure, *dbClient.Data[environmentID].Status)
}

func TestProvisionCreatesAKubernetesDeployment(t *testing.T) {
	_, k8sClient, _, environmentID := clientSetup(false)
	require.Equal(t, environmentID, k8sClient.CreatedDeploymentName)
	require.Equal(t, "test", k8sClient.CreatedDeploymentImage)
}

func TestProvisionCreatesAKubernetesService(t *testing.T) {
	_, k8sClient, _, environmentID := clientSetup(false)
	require.Equal(t, environmentID, k8sClient.CreatedServiceName)
}

func TestProvisionCreatesTLSSecret(t *testing.T) {
	_, k8sClient, _, _ := clientSetup(false)
	require.True(t, k8sClient.TLSSecretCreated)
}

func TestProvisionCreatesSecret(t *testing.T) {
	_, k8sClient, _, environmentID := clientSetup(false)

	delete(k8sClient.CreatedSecretData, "PRIVATE_KEY")

	require.Equal(t, map[string]string{
		"REPO_URL":    "http://gitlab.com/shekharpatnaik/test",
		"REF":         "main",
		"REPO_DIR":    "test",
		"SSH_COMMAND": fmt.Sprintf("ssh -i [PATH TO PRIVATE KEY] %s@ssh..", environmentID),
	}, k8sClient.CreatedSecretData)
}

func TestProvisionCreatesAKubernetesIngress(t *testing.T) {
	_, k8sClient, _, environmentID := clientSetup(false)
	require.Equal(t, environmentID, k8sClient.CreatedIngressName)
}

func TestProvisionCreatesDNSRecord(t *testing.T) {
	_, _, dnsClient, environmentID := clientSetup(false)
	require.Equal(t, "1.1.1.1", dnsClient.LastRequestIP)
	require.Equal(t, environmentID, dnsClient.LastRequestName)
}

func clientSetup(kubernetesFailure bool) (*db.MockDBClient, *k8s.MockK8sClient, *dns.MockDNSProvider, string) {
	dbClient := db.NewMockDBClient()
	k8sClient := &k8s.MockK8sClient{
		KubernetesFailure: kubernetesFailure,
	}

	dnsClient := &dns.MockDNSProvider{}

	ctx := context.Background()

	id := uuid.New().String()
	username := "patnaikshekhar"

	environmentID := fmt.Sprintf("%s-%s", username, id)

	provision.ProvisionEnvironment(ctx, id, k8sClient, dbClient, dnsClient, username, "", "", &environment.ExperimentsConfig{
		Version: "v1",
		Environments: []environment.Environment{
			{Image: "test"},
		},
	}, "randomcert", "randomcert",
		"http://gitlab.com/shekharpatnaik/test", "489899", "main", &auth.OAuth2Settings{}, true)

	return dbClient, k8sClient, dnsClient, environmentID
}
