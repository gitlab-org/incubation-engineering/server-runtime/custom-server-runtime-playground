package provision

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"time"

	"gitlab.com/patnaikshekhar/experiments/auth"
	"gitlab.com/patnaikshekhar/experiments/db"
	"gitlab.com/patnaikshekhar/experiments/dns"
	"gitlab.com/patnaikshekhar/experiments/environment"
	"gitlab.com/patnaikshekhar/experiments/k8s"
	"gitlab.com/patnaikshekhar/experiments/utils"
)

const (
	ingressServiceName          = "ingress-nginx"
	ingressServiceNamespace     = "ingress-nginx-controller"
	defaultServiceHTTPPort      = 80
	defaultServiceSSHPort       = 22
	defaultServiceTargetPort    = 4180
	defaultServiceSSHTargetPort = 22
	cookieSecretLength          = 16
)

func ProvisionEnvironment(
	ctx context.Context,
	name string,
	client k8s.Client,
	dbClient db.DB,
	dnsClient dns.DnsProvider,
	username string,
	baseDomain string,
	domainSuffix string,
	config environment.EnvironmentConfig,
	certFile string,
	keyFile string,
	repoUrl string,
	projectID string,
	ref string,
	oauthSettings *auth.OAuth2Settings,
	isTest bool) {

	environmentID := fmt.Sprintf("%s-%s", username, name)

	// Generate keys for SSH
	publicKey, privateKey, err := utils.GenKeyPair()
	if err != nil {
		log.Printf("Error in creating keys for environment %s", environmentID)
		return
	}

	// Create entry in Database
	err = dbClient.CreateEnvironment(ctx, environmentID, username, projectID, ref, string(publicKey))
	if err != nil {
		log.Printf("Error in creating environment %s in database", environmentID)
		return
	}
	updateSubStatus(ctx, dbClient, environmentID, "Started Provisioning")

	// Create Namespace
	err = client.CreateNamespace(ctx, environmentID)
	if err != nil {
		errorEnvironmentStatus(ctx, dbClient, environmentID, "Could not create namespace", err.Error())
		return
	}
	updateSubStatus(ctx, dbClient, environmentID, "Created namespace")

	// Create PVC
	err = client.CreatePVC(ctx, environmentID)
	if err != nil {
		errorEnvironmentStatus(ctx, dbClient, environmentID, "Could not create volume", err.Error())
		return
	}
	updateSubStatus(ctx, dbClient, environmentID, "Created volume")

	// Create Secret for proxy
	err = client.CreateSecret(ctx, "proxy", environmentID, map[string]string{
		"CLIENT_ID":            oauthSettings.ClientID,
		"CLIENT_SECRET":        oauthSettings.ClientSecret,
		"REDIRECT_URI":         fmt.Sprintf("https://%s.%s/auth/redirect", domainSuffix, baseDomain),
		"WHITELIST_DOMAIN":     fmt.Sprintf("*.%s.%s", domainSuffix, baseDomain),
		"ISSUER_URL":           oauthSettings.BaseUrl,
		"COOKIE_SECRET":        utils.RandStringBytes(cookieSecretLength),
		"GLIDE_URL":            fmt.Sprintf("https://%s.%s", domainSuffix, baseDomain),
		"GLIDE_WORKSPACE_NAME": environmentID,
	})
	if err != nil {
		errorEnvironmentStatus(ctx, dbClient, environmentID, "Could not create secret for IDE", err.Error())
		return
	}
	updateSubStatus(ctx, dbClient, environmentID, "Created secret for IDE")

	// Create Secret for IDE
	err = client.CreateSecret(ctx, environmentID, environmentID, map[string]string{
		"REPO_URL":    repoUrl,
		"REF":         ref,
		"REPO_DIR":    utils.GetDirectoryNameFromRepoUrl(repoUrl),
		"PRIVATE_KEY": privateKey,
		"SSH_COMMAND": fmt.Sprintf("ssh -i [PATH TO PRIVATE KEY] %s@ssh.%s.%s", environmentID, domainSuffix, baseDomain),
	})
	if err != nil {
		errorEnvironmentStatus(ctx, dbClient, environmentID, "Could not create secret for IDE", err.Error())
		return
	}
	updateSubStatus(ctx, dbClient, environmentID, "Created secret for IDE")

	// Create Deployment
	err = client.CreateDeployment(ctx, environmentID, environmentID, config)
	if err != nil {
		errorEnvironmentStatus(ctx, dbClient, environmentID, "Could not create deployment", err.Error())
		return
	}
	updateSubStatus(ctx, dbClient, environmentID, "Created deployment")

	// Create Service SSH
	err = client.CreateService(ctx, fmt.Sprintf("%s-ssh", environmentID), environmentID, defaultServiceSSHPort, defaultServiceSSHTargetPort)
	if err != nil {
		errorEnvironmentStatus(ctx, dbClient, environmentID, "Could not create service", err.Error())
		return
	}
	updateSubStatus(ctx, dbClient, environmentID, "Created service for SSH")

	// Create Service
	err = client.CreateService(ctx, environmentID, environmentID, defaultServiceHTTPPort, defaultServiceTargetPort)
	if err != nil {
		errorEnvironmentStatus(ctx, dbClient, environmentID, "Could not create service", err.Error())
		return
	}
	updateSubStatus(ctx, dbClient, environmentID, "Created service for HTTP")

	// Create service for each of the ports
	for _, port := range config.GetPorts() {
		name := fmt.Sprintf("%s-%d-%s", username, port, name)

		err = client.CreateService(ctx, name, environmentID, defaultServiceHTTPPort, port)
		if err != nil {
			errorEnvironmentStatus(ctx, dbClient, environmentID,
				fmt.Sprintf("Could not create service for port %d", port), err.Error())
			return
		}
		updateSubStatus(ctx, dbClient, environmentID, fmt.Sprintf("Created service for port %d", port))
	}

	// Create TLS Secret
	err = client.CreateTLSSecret(ctx, environmentID, certFile, keyFile)
	if err != nil {
		errorEnvironmentStatus(ctx, dbClient, environmentID, "Could not create TLS certificates", err.Error())
		return
	}
	updateSubStatus(ctx, dbClient, environmentID, "Created TLS certificates")

	// Create Ingress
	err = client.CreateIngress(ctx, environmentID, environmentID, baseDomain, domainSuffix)
	if err != nil {
		errorEnvironmentStatus(ctx, dbClient, environmentID, "Could not create ingress", err.Error())
		return
	}
	updateSubStatus(ctx, dbClient, environmentID, "Created ingress")

	// Create Ingress for each port
	for _, port := range config.GetPorts() {
		name := fmt.Sprintf("%s-%d-%s", username, port, name)
		err = client.CreateIngress(ctx, name, environmentID, baseDomain, domainSuffix)
		if err != nil {
			errorEnvironmentStatus(ctx, dbClient, environmentID,
				fmt.Sprintf("Could not create ingress for port %d", port), err.Error())
			return
		}
		updateSubStatus(ctx, dbClient, environmentID, fmt.Sprintf("Created ingress for port %d", port))
	}

	// Get IP of service
	loadbalancerIP, err := client.GetServiceIP(ctx, ingressServiceName, ingressServiceNamespace)
	if err != nil {
		errorEnvironmentStatus(ctx, dbClient, environmentID, "Could not find ip for ingress", err.Error())
		return
	}

	// Provision DNS
	err = dnsClient.CreateDnsRecord(ctx, environmentID, loadbalancerIP)
	if err != nil {
		errorEnvironmentStatus(ctx, dbClient, environmentID, "Could not create DNS record", err.Error())
		return
	}
	updateSubStatus(ctx, dbClient, environmentID, "Created DNS record")

	// Provision DNS for ports
	for _, port := range config.GetPorts() {
		name := fmt.Sprintf("%s-%d-%s", username, port, name)
		err = dnsClient.CreateDnsRecord(ctx, name, loadbalancerIP)
		if err != nil {
			errorEnvironmentStatus(ctx, dbClient, environmentID, fmt.Sprintf("Could not create DNS record for port %d", port), err.Error())
			return
		}
		updateSubStatus(ctx, dbClient, environmentID, "Waiting for pod to start...")
	}

	// Test if deployment is running
	var status k8s.DeploymentStatus
	for {
		status, err = client.GetDeploymentStatus(ctx, environmentID)
		if err != nil {
			errorEnvironmentStatus(ctx, dbClient, environmentID, "Could not check status of job", err.Error())
			return
		}

		if status != k8s.DeploymentStatusNotStarted {
			break
		}

		time.Sleep(2 * time.Second)
	}

	if status == k8s.DeploymentStatusError {
		errorEnvironmentStatus(ctx, dbClient, environmentID, "Failed to run pod", "Unknown error")
		return
	}

	updateSubStatus(ctx, dbClient, environmentID, "Waiting for site to become reachable....")
	// Test if site is reachable
	if !isTest {
		err = waitForSiteToBecomeReachable(ctx, dbClient, environmentID, baseDomain, domainSuffix)
		if err != nil {
			errorEnvironmentStatus(ctx, dbClient, environmentID, "Could not reach site", err.Error())
			return
		}
	}

	// Update status to success
	finalUrl := fmt.Sprintf(
		"https://%s.%s.%s?folder=/home/workspaces/%s",
		environmentID, domainSuffix, baseDomain, utils.GetDirectoryNameFromRepoUrl(repoUrl))
	dbClient.UpdateEnvironmentStatus(ctx, environmentID, db.EnvironmentStatusSuccess, "", "", finalUrl)
	if err != nil {
		log.Printf("Error updating database %s", err)
	}
}

func errorEnvironmentStatus(ctx context.Context, c db.DB, id string, message string, errorMessage string) {
	err := c.UpdateEnvironmentStatus(ctx, id, db.EnvironmentStatusFailure, message, errorMessage, "")
	if err != nil {
		log.Printf("Error updating database %s", err)
	}
}

func updateSubStatus(ctx context.Context, c db.DB, id string, message string) {
	err := c.UpdateEnvironmentStatus(ctx, id, db.EnvironmentStatusInProgress, message, "", "")
	if err != nil {
		log.Printf("Error updating database %s", err)
	}
}

func waitForSiteToBecomeReachable(ctx context.Context, d db.DB, environmentID string, baseDomain string, domainSuffix string) error {
	url := fmt.Sprintf("https://%s.%s.%s", environmentID, domainSuffix, baseDomain)
	errCount := 0

	for {
		time.Sleep(1 * time.Minute)

		res, err := http.Get(url)
		if err != nil {
			errCount += 1
			if errCount > 20 {
				return err
			}
		} else if res.StatusCode < 400 || res.StatusCode == 403 {
			break
		}
	}

	return nil
}
