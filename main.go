package main

import (
	"gitlab.com/gitlab-org/labkit/tracing"
	"gitlab.com/patnaikshekhar/experiments/cmd"
)

func main() {
	tracing.Initialize(tracing.WithServiceName("glide"))
	cmd.Execute()
}
